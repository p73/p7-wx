import { Image, Picker, Text, View } from 'remax/wechat'
import styles from './index.css'
import arrowDownIcon from '../../assets/icons/arrowdown.svg'
import * as React from 'react'
import { useEffect } from 'react'
import { config } from '../../library/config'
import { couponDataConstants, dbCollections } from '../../library/constants'
import Divider from '../divider'
import Card from '../card'
import { useDb } from '../../library/db'
import { requestGet } from '../../library/wxPromise'
import { verifyToken } from '../../library/user'
import { getTotalMemberPoints } from '../../library/util'

export default ({
  originalPrice,
  cost,
  setIsLoading,
  selectedCoupon,
  setSelectedCoupon,
  hours = 1,
  caloriesBurnRate,
  couponTypeFilter
}) => {
  const [couponData, setDefaultCouponData] = useDb(dbCollections.couponDatas)
  const [userInventory, setDefaultUserInventory] = useDb(dbCollections.userInventory)

  const filterUserInventory = ({ quantity, coupon }) =>
    coupon &&
    coupon.type !== couponTypeFilter &&
    coupon.id !== config.app.memberPointCoupon &&
    coupon.id !== config.app.memberPointUnredeemableCoupon &&
    quantity > 0
  useEffect(() => {
    async function _ () {
      setIsLoading(true)
      const token = await verifyToken()
      await setDefaultCouponData(() => requestGet(`/coupon-data/1`))
      let userInventory = await setDefaultUserInventory(() => requestGet(`/user-inventories`, {
        Authorization: `Bearer ${token}`
      }), true)
      setSelectedCoupon(userInventory.findOne(filterUserInventory))
      setIsLoading(false)

    }

    _()
  }, [])

  return <Card className={styles.paymentCard}>
    <View className={styles.paymentCardTop}>
      <View className={styles.paymentCardTopLeft}>
        {hours}h消耗{hours * caloriesBurnRate}卡路里
      </View>
      <View className={styles.paymentCardTopRight}>
        VIP价 ¥{Math.round(originalPrice * config.app.memberDiscount)} | <Text
        className={styles.classDetailsCardFooterRightNewMemberPrice}>新会员
        <Text className={styles.classDetailsCardFooterRightNewMemberPriceNumber}>
          ¥{originalPrice > couponData.findOne(couponDataConstants.defaultNewUserCouponData)?.flat_discount ? originalPrice - couponData.findOne(couponDataConstants.defaultNewUserCouponData)?.flat_discount : 0}
        </Text>
      </Text>
      </View>
    </View>

    <Divider className={styles.paymentCardDivider}/>
    <View className={styles.paymentCardBottom}>
      <View className={styles.paymentCardBottomEntry}>
        <View className={styles.paymentCardBottomLabel}>
          优惠
        </View>
        <Picker
          mode="selector"
          onColumnChange={setSelectedCoupon}
          onChange={({ detail }) => {
            setSelectedCoupon(userInventory.find(filterUserInventory)[+detail.value])
          }}
          range-key={'name'}
          range={userInventory.find(filterUserInventory)?.map(inventory => {
            inventory.name = inventory.coupon.name
            return inventory
          })}
        >
          <Text className={styles.paymentCardBottomEntryCouponSelector}>
            {selectedCoupon ? selectedCoupon?.coupon?.name : '无代金券'}
          </Text>
          <Image src={arrowDownIcon} className={styles.paymentCardBottomEntryCouponSelectorIcon}/>
        </Picker>
      </View>
      <View className={styles.paymentCardBottomEntryModifier}/>
      {userInventory
        .find(({ coupon }) => coupon.id === config.app.memberPointCoupon || coupon.id === config.app.memberPointUnredeemableCoupon).length ?
        <>
          <View className={styles.paymentCardBottomEntry}>
            <View className={styles.paymentCardBottomLabel}>
              钱包
            </View>
            <Text className={styles.paymentCardBottomEntryCouponSelector}>
              ¥{getTotalMemberPoints(userInventory)}
            </Text>
          </View>
          <View className={styles.paymentCardBottomEntryModifier}/>
        </> : null
      }
      <View className={styles.paymentCardBottomEntry}>
        <View className={styles.paymentCardBottomEntryPrice}>
          <View className={styles.paymentCardBottomEntryPriceLeft}>
            <Text className={styles.paymentCardBottomEntryPriceLeftText}>
              原价
            </Text> <Text className={styles.paymentCardBottomEntryPriceLeftNumber}>
            ¥{originalPrice}
          </Text>
          </View>
          <View className={styles.paymentCardBottomEntryPriceRight}>
            还需微信支付 <Text className={styles.paymentCardBottomEntryPriceRightNumber}>
            ¥{Math.round((getTotalMemberPoints(userInventory) > cost) ? 0 : cost - getTotalMemberPoints(userInventory))}
          </Text>
          </View>
        </View>
      </View>
    </View>
  </Card>
}
