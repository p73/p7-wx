import { Image, Picker, Text } from 'remax/wechat'
import { placeSelectionBuilder } from '../../library/places'
import styles from './index.css'
import arrowDownIcon from '../../assets/icons/arrowdown.svg'
import * as React from 'react'

export default ({ publicSpacesList, selectedCityFilter, setSelectedCityFilter }) => {
  return <Picker
    mode="selector"
    onChange={({ detail }) => {
      setSelectedCityFilter(placeSelectionBuilder(publicSpacesList)[+detail.value])
    }}
    onColumnChange={setSelectedCityFilter}
    range-key={'name'}
    range={placeSelectionBuilder(publicSpacesList)}
  >
    <Text className={styles.headerSelectorText}>
      {selectedCityFilter.name}
    </Text>
    <Image src={arrowDownIcon} className={styles.headerSelectorIcon}/>
  </Picker>
}
