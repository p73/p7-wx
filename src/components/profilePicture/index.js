import * as React from 'react'
import { Image } from 'remax/wechat'
import styles from './index.css'
import { requestStatic } from '../../library/wxPromise'

export default ({ src, externalSrc }) => {
  return <Image src={externalSrc || requestStatic(src)}
                className={styles.root}/>
};
