import * as React from 'react'
import { Image, View } from 'remax/wechat'
import styles from './index.css'

export default ({ backgroundImageSrc, children, ...props }) => {
  return <View {...props} className={styles.root}>
    <Image src={backgroundImageSrc} className={styles.backgroundImage}/>
  </View>
}
