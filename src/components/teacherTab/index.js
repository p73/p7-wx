import * as React from 'react'
import { Image, redirectTo, View } from 'remax/wechat'
import { usePageInstance } from 'remax'
import styles from './index.css'
import calendarIcon from '../../assets/icons/calendar.svg'
import homeIcon from '../../assets/icons/home.svg'
import homeActiveIcon from '../../assets/icons/home_black.svg'
import calendarActiveIcon from '../../assets/icons/calendar_black.svg'
import extendedArrowLeftIcon from '../../assets/icons/extendedarrowleft.svg'

export default () => {
  const instance = usePageInstance()

  return (
    <View className={styles.root}>
      {
        [
          { page: 'pages/profilePage/index', inactiveIcon: extendedArrowLeftIcon, activeIcon: extendedArrowLeftIcon },
          { page: 'pages/teacherCalendarPage/index', inactiveIcon: calendarIcon, activeIcon: calendarActiveIcon },
          { page: 'pages/teacherDaySelectionPage/index', inactiveIcon: homeIcon, activeIcon: homeActiveIcon },
        ].map(({ page, inactiveIcon, activeIcon, modifier, action }) => {
          return <View className={styles.tab} key={inactiveIcon} onClick={async () => {
            if (action) {
              await action()
            }
            await redirectTo({ url: '/' + page })
          }}>
            <Image src={instance.route === page ? activeIcon : inactiveIcon}
                   className={`${styles.tabIcon} ${modifier}`}/>
          </View>
        })
      }
    </View>
  )
};
