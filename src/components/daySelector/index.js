import styles from './index.css'
import { Image, Text, View } from 'remax/wechat'
import arrowLeftIcon from '../../assets/icons/arrowleft.svg'
import arrowRightIcon from '../../assets/icons/arrowright.svg'
import Card from '../card'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { addDays, isSameDateWithoutTime } from '../../library/date'
import { chineseDays } from '../../library/constants'

export default ({ currentDay, setCurrentDay }) => {
  const [daysList, setDaysList] = useState([])

  const onClickArrowLeft = () => {
    setCurrentDay(addDays(currentDay, -1))
  }
  const onClickArrowRight = () => {
    setCurrentDay(addDays(currentDay, 1))
  }

  useEffect(() => {
    const today = currentDay || new Date()
    setCurrentDay(today)
    let tempDaysList = []
    for (let i = today.getDay(); i >= 0; i--) {
      tempDaysList.push(addDays(today, i - today.getDay()))
    }
    tempDaysList.reverse()
    for (let i = today.getDay() + 1; i <= 6; i++) {
      tempDaysList.push(addDays(today, i - today.getDay()))
    }
    setDaysList(tempDaysList)
  }, [currentDay])

  if (!currentDay) return null

  return <Card className={styles.daySelector}>
    <View className={styles.daySelectorDay}>
      {
        chineseDays.map((day, index) => {
          const isSameDay = currentDay.getDay() === index
          return <View key={Math.random()} className={styles.headerRowBox}
                       onClick={() => {
                         setCurrentDay(addDays(currentDay, index - currentDay.getDay()))
                       }}>
            {
              isSameDay ? <View
                className={styles.headerRowBoxActiveBackgroundTop}
              /> : null
            }
            <View
              style={{
                color: isSameDay ? '#fff' : 'inherit'
              }}
              className={styles.headerRowBoxTextTop}>
              {day}
            </View>
          </View>
        })
      }
    </View>
    <View className={styles.daySelectorDate}>
      {
        daysList.map(day => {
          const isSameDay = isSameDateWithoutTime(day, currentDay)
          return <View
            key={Math.random()}
            className={styles.headerRowBox}
            style={{
              color: isSameDay ? '#fff' : 'inherit'
            }}
            onClick={() => {
              setCurrentDay(day)
            }}
          >
            {
              isSameDay ? <View
                className={styles.headerRowBoxActiveBackgroundBottom}
              /> : null
            }
            <Text
              className={styles.headerRowBoxTextBottom}
            >
              {day.getMonth() + 1}.{day.getDate()}
            </Text>
          </View>
        })
      }
      <View className={styles.headerLastRowArrowLeft} onClick={onClickArrowLeft}>
        <Image className={styles.headerLastRowArrowIcon} src={arrowLeftIcon}/>
      </View>

      <View className={styles.headerLastRowArrowRight} onClick={onClickArrowRight}>
        <Image className={styles.headerLastRowArrowIcon} src={arrowRightIcon}/>
      </View>
    </View>
  </Card>
}
