import * as React from 'react'
import { View } from 'remax/wechat'
import styles from './index.css'

export default ({ children, height = 65 }) => {
  return <View className={styles.root} style={{
    height: `${height}vh`
  }}>
    {children}
  </View>
}
