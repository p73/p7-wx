import * as React from 'react'
import { Image, redirectTo, View } from 'remax/wechat'
import { usePageInstance } from 'remax'
import styles from './index.css'
import calendarIcon from '../../assets/icons/calendar.svg'
import homeIcon from '../../assets/icons/home.svg'
import placeIcon from '../../assets/icons/place.svg'
import personIcon from '../../assets/icons/person.svg'
import calendarActiveIcon from '../../assets/icons/calendar_black.svg'
import homeActiveIcon from '../../assets/icons/home_black.svg'
import placeActiveIcon from '../../assets/icons/place_black.svg'
import personActiveIcon from '../../assets/icons/person_black.svg'
import { requestWxUserProfile } from '../../library/wxPromise'
import { hasNotch } from '../../library/util'

export default () => {
  const instance = usePageInstance()

  return (
    <View className={styles.root} style={{
      paddingBottom: hasNotch() ? '1vw' : 0
    }}>
      {
        [
          { page: 'pages/mainPage/index', inactiveIcon: placeIcon, activeIcon: placeActiveIcon },
          { page: 'pages/placePage/index', inactiveIcon: homeIcon, activeIcon: homeActiveIcon },
          { page: 'pages/calendarPage/index', inactiveIcon: calendarIcon, activeIcon: calendarActiveIcon },
          {
            page: 'pages/profilePage/index',
            inactiveIcon: personIcon,
            activeIcon: personActiveIcon,
            modifier: styles.tabIconLarger,
            action: async () => requestWxUserProfile()
          },
        ].map(({ page, inactiveIcon, activeIcon, modifier, action }) => {
          return <View className={styles.tab}
                       key={inactiveIcon}
                       style={{
                         paddingBottom: hasNotch() ? '8vw' : '5vw'
                       }}
                       onClick={async () => {
                         if (action) {
                           await action()
                         }
                         await redirectTo({ url: '/' + page })
                       }}
          >
            <Image src={instance.route === page ? activeIcon : inactiveIcon}
                   className={`${styles.tabIcon} ${modifier}`}/>
          </View>
        })
      }
    </View>
  )
};
