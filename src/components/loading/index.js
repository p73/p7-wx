import * as React from 'react'
import { Image, View } from 'remax/wechat'
import styles from './index.css'
import loadingIcon from '../../assets/images/loading.svg'

export default ({ loading }) => {
  if (!loading) {
    return null
  }
  return (
    <View className={styles.root}>
      <Image src={loadingIcon} className={styles.loadingIcon}/>
    </View>
  )
};
