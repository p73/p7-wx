import * as React from 'react'
import { useEffect, useState } from 'react'
import { Image, navigateBack, View } from 'remax/wechat'
import styles from './index.css'
import extendedArrowLeftIcon from '../../assets/icons/extendedarrowleft.svg'
import { verifyToken } from '../../library/user'
import LoadingScreen from '../loading'
import Card from '../card'
import Markdown from 'react-markdown'

import { useShareAppMessage } from '../../library/hooks'

export default ({ loaderCallback, noWrapper = false }) => {
  const [text, setText] = useState('')
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    async function _ () {
      setIsLoading(true)
      const token = await verifyToken()
      setText(await loaderCallback(token))
      setIsLoading(false)
    }

    _()
  }, [])

  useShareAppMessage(setIsLoading)

  const card = <Card>
    <Markdown components={{
      a: ({ children }) => <View>{children}</View>,
      blockquote: ({ children }) => <View>{children}</View>,
      code: ({ children }) => <View>{children}</View>,
      em: ({ children }) => <View>{children}</View>,
      h1: ({ children }) => <View className={styles.h1}>{children}</View>,
      h2: ({ children }) => <View>{children}</View>,
      h3: ({ children }) => <View>{children}</View>,
      h4: ({ children }) => <View>{children}</View>,
      h5: ({ children }) => <View>{children}</View>,
      h6: ({ children }) => <View>{children}</View>,
      hr: ({ children }) => <View>{children}</View>,
      img: ({ children }) => <View>{children}</View>,
      li: ({ children, node, ...props }) => <View className={styles.li}>{props.index + 1}. {children}</View>,
      ol: ({ node, children, ...props }) => <View>{children}</View>,
      p: ({ children }) => <View className={styles.p}>{children}</View>,
      pre: ({ children }) => <View>{children}</View>,
      strong: ({ children }) => <View>{children}</View>,
      ul: ({ children }) => <View>{children}</View>,
    }} children={text} key={text}/>
  </Card>

  if (noWrapper) {
    return card
  }

  return (
    <View className={styles.root}>
      <View className={styles.headerBackButton} onClick={() => navigateBack()}>
        <Image src={extendedArrowLeftIcon} className={styles.headerBackButtonIcon}/>
        Back
      </View>
      <View className={styles.body}>
        {card}
      </View>
      <LoadingScreen loading={isLoading}/>
    </View>
  )
};
