import * as React from 'react'
import { View } from 'remax/wechat'
import styles from './index.css'

export default ({ question = '', selection = [], selectedAnswers = [], onSelectedAnswer = () => {} }) => {
  return <View className={styles.root}>
    <View className={styles.title}>
      {question}
    </View>
    <View className={`${styles.selections} ${selection.length > 2 ? styles.selectionsVertical : ''}`}>
      {
        selection.map(({ id, content }) => {
          const isSelected = selectedAnswers.includes(id)
          return <View
            key={id}
            onClick={() => onSelectedAnswer(id)}
            className={isSelected ? styles.selectionsElementSelected : styles.selectionsElement}
          >
            <View className={isSelected ? styles.circleSelected : styles.circle}/>{content}
          </View>
        })
      }
    </View>
  </View>
}
