import * as React from 'react'
import { useEffect, useState } from 'react'
import { Image, View } from 'remax/wechat'
import styles from './index.css'
import { useNativeEffect } from 'remax'
import logo from '../../assets/icons/logo.jpg'
import { sleep } from '../../library/promise'
import loadingIcon from '../../assets/images/loading.svg'

export default ({ show = true, shouldTrigger }) => {
  if (!shouldTrigger) return null
  const [animationComplete, setAnimationComplete] = useState(false)
  const [isDone, setIsDone] = useState(false)
  useNativeEffect(() => {
    async function _ () {
      await sleep(1500)
      setAnimationComplete(true)
    }

    _()
  })
  useEffect(() => {
    if (animationComplete && !isDone) {
      setIsDone(!show)
    }
  }, [animationComplete, show, isDone])
  // if (isDone) {
  //   return null
  // }
  return <View className={`${styles.root} ${isDone ? styles.rootExit : ''}`}>
    <Image src={logo} className={styles.image}/>
    <View className={styles.text1}>
      P7 Community
    </View>
    <View className={styles.text2}>
      按次付费
    </View>
    <View className={styles.text3}>
      自由舞动
    </View>
    {animationComplete ?
      <Image src={loadingIcon} className={styles.loadingIcon}/> :
      null
    }
  </View>
}
