import styles from './index.css'
import { Text, View } from 'remax/wechat'
import { formatTimeElement } from '../../library/date'
import EmptySpace from '../emptySpace'
import * as React from 'react'

export default ({ expirationDate, flatDiscount, couponName, ...props }) => {
  return <View className={styles.coupon} {...props}>
    <View className={styles.couponCircleBackground}/>
    {couponName ?
      <View className={styles.couponNumberLabel}>
        <Text className={styles.couponNumberLabelNumber}>{couponName}</Text>
      </View> : <View className={styles.couponNumberLabel}>
        <Text className={styles.couponNumberLabelNumber}>{flatDiscount}</Text> 元券
      </View>}

    <View className={styles.couponExpiry}>
      <Text>
        {expirationDate.getFullYear()}.{formatTimeElement(expirationDate.getMonth() + 1)}.{formatTimeElement(expirationDate.getDate())}前可使用
      </Text>
      <EmptySpace/>
      <Text>
        过期无效
      </Text>
    </View>
    <View className={styles.couponActionButton}>
      立即使用
    </View>
  </View>
}
