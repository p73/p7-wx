import styles from './index.css'
import { Image, View } from 'remax/wechat'
import arrowLeftIcon from '../../assets/icons/arrowleft.svg'
import arrowRightIcon from '../../assets/icons/arrowright.svg'
import Card from '../card'
import * as React from 'react'
import { useEffect, useState } from 'react'
import { addDays } from '../../library/date'
import { chineseDays } from '../../library/constants'

export default ({ title, currentDay = 0, setCurrentDay }) => {
  const onClickArrowLeft = () => {
    setCurrentDay((currentDay - 1) % 7)
  }
  const onClickArrowRight = () => {
    setCurrentDay((currentDay + 1) % 7)
  }

  return <Card className={styles.daySelector}>
    <View className={styles.daySelectorTitle}>
      {title}
    </View>
    <View className={styles.daySelectorDay}>
      {
        chineseDays.map((day, index) => {
          const isSameDay = currentDay === index
          return <View key={Math.random()} className={styles.headerRowBox}
                       onClick={() => {
                         setCurrentDay(index)
                       }}>
            {
              isSameDay ? <View
                className={styles.headerRowBoxActiveBackgroundTop}
              /> : null
            }
            <View
              style={{
                color: isSameDay ? '#fff' : 'inherit'
              }}
              className={styles.headerRowBoxTextTop}>
              {day}
            </View>
          </View>
        })
      }
      <View className={styles.headerLastRowArrowLeft} onClick={onClickArrowLeft}>
        <Image className={styles.headerLastRowArrowIcon} src={arrowLeftIcon}/>
      </View>

      <View className={styles.headerLastRowArrowRight} onClick={onClickArrowRight}>
        <Image className={styles.headerLastRowArrowIcon} src={arrowRightIcon}/>
      </View>
    </View>
  </Card>
}
