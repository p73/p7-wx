import * as React from 'react'
import { Image } from 'remax/wechat'
import { requestStatic } from '../../library/wxPromise'
import styles from './index.css'

export default ({ src, externalSrc }) => {
  return <Image src={externalSrc || requestStatic(src)} className={styles.root}/>
};
