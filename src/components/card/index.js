import * as React from 'react'
import styles from './index.css'
import { View } from 'remax/wechat'

export default ({children, className, ...props}) => {
  return <View className={`${styles.root} ${className}`} {...props}>
    {children}
  </View>
}
