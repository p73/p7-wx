import * as React from 'react'
import styles from './index.css'
import { Text, View } from 'remax/wechat'
import { hasNotch } from '../../library/util'

export default ({ title, headerClassname, children }) => {
  return <View className={`${styles.header} ${headerClassname}`} style={{
    marginTop: hasNotch() > 20 ? 0 : '-24rpx'
  }}>
    <View className={styles.headerTopRow}>
      {
        typeof title === 'string' ?
          <Text className={styles.headerTopRowText}>
            {title}
          </Text> : title
      }

    </View>
    <View className={styles.headerBody}>
      {children}
    </View>
  </View>
}
