import styles from './index.css'
import { Image, Video, View } from 'remax/wechat'
import * as React from 'react'
import { useState } from 'react'
import loadingIcon from '../../assets/images/loading.svg'

export default ({ src, ...props }) => {
  const [showOverlay, setShowOverlay] = useState(true)
  const [progress, setProgress] = useState(1)
  const [isFullScreen, setIsFullScreen] = useState(false)
  return <View className={styles.headerMedia} {...props}>
    <Video src={src} className={styles.headerMediaVideo}
           onFullScreenChange={e => {
             setIsFullScreen(e.detail.fullScreen)
           }}
           object-fit={isFullScreen ? 'contain' : 'fill'}
           direction={90}
           onLoadedMetaData={e => {
             setShowOverlay(false)
           }}
           onProgress={e => {
             if (progress < 100) {
               setProgress(progress + 1)
             } else {
               setProgress(99)
             }
           }}
    />
    {
      showOverlay ? <View className={styles.headerMediaVideoOverlay}>
        <Image src={loadingIcon} className={styles.loadingIcon}/>
        <View className={styles.headerMediaVideoOverlayText}>
          加载中 ({Math.round(Math.sqrt(progress) * 10)}%)
        </View>
      </View> : null
    }

  </View>
}
