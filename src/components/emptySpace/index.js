import * as React from 'react'
import styles from './index.css'
import { View } from 'remax/wechat'

export default ({ horizontal, vertical, scaleToWidth = false, scaleToHeight = false, className, ...props }) => {
  return <View className={`${styles.root} ${className}`} {...props} style={{
    paddingLeft: `${horizontal}${scaleToWidth ? 'vw' : 'rpx'}`,
    paddingTop: `${vertical}${scaleToHeight ? 'vh' : 'rpx'}`
  }}/>
}
