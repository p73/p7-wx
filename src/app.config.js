module.exports = {
  pages: [
    'pages/mainPage/index',
    'pages/buyMembershipPage/index',
    'pages/spaceDetailsPage/index',
    'pages/placePage/index',
    'pages/profilePage/index',
    'pages/calendarPage/index',
    'pages/paymentCompletePage/index',
    'pages/classDetailsPage/index',
    'pages/classGroupDetailsPage/index',
    'pages/thingsToNotePage/index',
    'pages/privacyStatementPage/index',
    'pages/teacherCalendarPage/index',
    'pages/teacherClassPage/index',
    'pages/teacherDaySelectionPage/index',
    'pages/specialCouponPage/index',
  ],
  window: {
    navigationBarTitleText: '',
    navigationBarBackgroundColor: '#fff',
    navigationStyle: 'custom'
  },
}
