import * as React from 'react'
import { useEffect, useState } from 'react'
import { Image, redirectTo, Text, View } from 'remax/wechat'
import styles from './index.css'
import extendedArrowLeftIcon from '../../assets/icons/extendedarrowleft.svg'
import { useQuery } from 'remax'
import { requestGet, requestPost, requestPut } from '../../library/wxPromise'
import { formatTimeElement } from '../../library/date'
import { verifyToken } from '../../library/user'
import LoadingScreen from '../../components/loading'
import ProfilePicture from '../../components/profilePicture'
import { useShareAppMessage } from '../../library/hooks'
import { useDb } from '../../library/db'
import { dbCollections } from '../../library/constants'
import { handleErrorLogging } from '../../library/error'

export default () => {
  let { classId } = useQuery()
  const [isLoading, setIsLoading] = useState(false)
  const [reservationPublicSpace, setReservationPublicSpace] = useState()
  const [, setDefaultReservation] = useDb(dbCollections.reservations)
  const [roomDatas, setDefaultRoomDatas] = useDb(dbCollections.roomDatas)
  const [publicSpaces, setDefaultPublicSpaces] = useDb(dbCollections.publicSpaces)
  const [learningClasses, setDefaultLearningClasses] = useDb(dbCollections.learningClasses)
  const [loginRecord, setDefaultLoginRecord] = useDb(dbCollections.loginRecord)
  const [targetLearningClass, setTargetLearningClass] = useState(null)
  const [targetLoginRecord, setTargetLoginRecord] = useState(null)

  useEffect(() => {
    async function _ () {
      setIsLoading(true)
      try {
        const token = await verifyToken()
        await setDefaultRoomDatas(() => requestGet(`/room-data`))
        const publicSpaces = await setDefaultPublicSpaces(() => requestGet(`/public-spaces`))
        const learningClasses = await setDefaultLearningClasses(() => requestGet(`/learning-classes`), true)
        setTargetLearningClass(learningClasses.findOne(classId))
        // check if login record has been created
        const previousLoginRecord = await requestGet(`/login-records/one?learning_class=${classId}`, {
          Authorization: `Bearer ${token}`
        })
        if (!previousLoginRecord?.learning_class) {
          setTargetLoginRecord(await requestPost(`/login-records`, {
            learning_class: classId
          }, {
            Authorization: `Bearer ${token}`
          }))
        } else {
          setTargetLoginRecord(previousLoginRecord)
        }
      } catch (e) {
        handleErrorLogging(e)
      }
      setIsLoading(false)
    }

    _()
  }, [classId])

  const onConfirm = async () => {
    const token = await verifyToken()
    await requestPut(`/learning-classes/${classId}`, {
      is_teacher_confirmed: true
    }, {
      Authorization: `Bearer ${token}`
    })
    setTargetLearningClass({
      ...targetLearningClass,
      is_teacher_confirmed: true
    })
  }

  useShareAppMessage(setIsLoading)

  return (
    <View className={styles.root}>
      <View className={styles.headerBackButton} onClick={() => redirectTo({ url: '/pages/teacherCalendarPage/index' })}>
        <Image src={extendedArrowLeftIcon} className={styles.headerBackButtonIcon}/>
        Back
      </View>
      <View className={styles.body}>
        <View className={styles.bodyHeader}>
          <View className={styles.bodyHeaderLeft}>
            <View className={styles.bodyHeaderLeftTitle}>
              <View>
                <Text>
                  {targetLearningClass?.name}
                </Text>
                <Text
                  className={styles.bodyHeaderLeftSubTitle}>{targetLearningClass?.teacher.username}
                </Text>
              </View>
            </View>
            <View className={styles.bodyHeaderLeftTime}>
              {targetLearningClass?.starting_datetime ?
                <Text>
                  {(new Date(targetLearningClass?.starting_datetime)).getMonth() + 1}月{(new Date(targetLearningClass?.starting_datetime)).getDate()}日 {(new Date(targetLearningClass?.starting_datetime)).getHours()}:{formatTimeElement((new Date(targetLearningClass?.starting_datetime)).getMinutes())}-{(new Date(targetLearningClass?.ending_datetime)).getHours()}:{formatTimeElement((new Date(targetLearningClass?.ending_datetime)).getMinutes())}
                </Text>
                : null}
            </View>
            <View className={styles.bodyHeaderLeftAddress}>
              {targetLearningClass?.public_space?.name}
            </View>
          </View>
          <View className={styles.bodyHeaderRight}>
            <ProfilePicture
              src={targetLearningClass?.teacher?.profile_picture?.url}/>
          </View>
        </View>
        <View className={styles.bodyContent}>
          开课前15分钟、课程结束扫码进出签到
          <Image className={styles.bodyContentQRCode} src={targetLoginRecord?.qrcode}/>
        </View>

        <View className={styles.footer}>
          <View
            className={styles.actionButtonReserve}
            style={{
              background: targetLearningClass?.is_teacher_confirmed ? '#000' : '#45D0C0'
            }}
            onClick={targetLearningClass?.is_teacher_confirmed ? () => {} : onConfirm}
          >
            {
              targetLearningClass?.is_teacher_confirmed ? '已确认' : '确认上课'
            }
          </View>
        </View>
      </View>
      <LoadingScreen loading={isLoading}/>
    </View>
  )
};
