import * as React from 'react'
import { useEffect, useMemo, useState } from 'react'
import { Image, navigateTo, redirectTo, Text, View } from 'remax/wechat'
import styles from './index.css'

import extendedArrowLeftIcon from '../../assets/icons/extendedarrowleft.svg'
import arrowDownIcon from '../../assets/icons/arrowdown.svg'

import { useQuery } from 'remax'
import { requestGet, requestPost, requestStatic, requestWxShowToast } from '../../library/wxPromise'
import { formatTimeElement } from '../../library/date'
import { verifyToken } from '../../library/user'
import LoadingScreen from '../../components/loading'
import CustomVideo from '../../components/customVideo'
import Card from '../../components/card'
import ProfilePicture from '../../components/profilePicture'
import { useShareAppMessage } from '../../library/hooks'
import { useDb } from '../../library/db'
import { chineseDays, dbCollections } from '../../library/constants'
import { config } from '../../library/config'
import {
  computeFinalCost,
  findBoughtClassGroups,
  handleErrorAsJson,
  payWithPoints,
  requestSubscribedMessages
} from '../../library/util'
import PaymentCard from '../../components/paymentCard'

export default () => {
  const [publicSpaces, setDefaultPublicSpaces] = useDb(dbCollections.publicSpaces)
  const [couponData, setDefaultCouponData] = useDb(dbCollections.couponDatas)
  const [learningClassGroups, setDefaultLearningClassGroups] = useDb(dbCollections.learningClassGroups)
  const [reservations, setDefaultReservations] = useDb(dbCollections.reservations)
  const [userInventory, setDefaultUserInventory] = useDb(dbCollections.userInventory)

  const [expandTeacherBackground, setExpandTeacherBackground] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [selectedCoupon, setSelectedCoupon] = useState([])
  const [activeClassGroupData, setActiveClassGroupData] = useState()
  const [publicSpace, setPublicSpace] = useState([])
  const [classCost, setClassCost] = useState(0)

  let { id: groupId } = useQuery()
  useEffect(() => {
    async function _ () {
      setIsLoading(true)
      const token = await verifyToken()
      await setDefaultPublicSpaces(() => requestGet(`/public-spaces`))
      const learningClassGroups = await setDefaultLearningClassGroups(() => requestGet(`/learning-classes/groups/${groupId}`), true)
      await setDefaultCouponData(() => requestGet(`/coupon-data/1`))
      await Promise.all([
        setDefaultUserInventory(() => requestGet(`/user-inventories`, {
          Authorization: `Bearer ${token}`
        }), true),
        await setDefaultReservations(() => requestGet(`/reservations`, {
          Authorization: `Bearer ${token}`
        }), true)
      ])

      const activeClass = learningClassGroups.findOne(groupId)
      setActiveClassGroupData(activeClass)
      setPublicSpace(publicSpaces.findOne(activeClass.public_space.id))

      setIsLoading(false)
    }

    _()
  }, [groupId])

  useShareAppMessage(setIsLoading)

  const unpaidClasses = useMemo(() => {

    let [_, learningClassGroupTemp] = findBoughtClassGroups(reservations.find(({ user, status }) => {
      const storedUsername = wx.getStorageSync(config.storageKey.wxOpenId)
      if (status === 'Paid') {
        return user === storedUsername || user?.username === storedUsername || user?.weixin_username === storedUsername
      }
      return false
    }), learningClassGroups)

    return learningClassGroupTemp[groupId]
  }, [reservations, learningClassGroups])

  const totalOriginalPrice = activeClassGroupData ? activeClassGroupData.learning_classes
    .filter(({ id }) => !!unpaidClasses[id])
    .map(({ price_data }) => price_data.original_price)
    .reduce((a, b) => a + b, 0) : 0

  useEffect(() => {
    setClassCost(computeFinalCost(userInventory, totalOriginalPrice, selectedCoupon))
  }, [selectedCoupon, userInventory, activeClassGroupData])

  const onClickSubscribe = async () => {
    requestSubscribedMessages()
    await requestPost('/user-form-answers', {
      form_question: config.app.formQuestions.classGroupSubscriptionId,
      freeform_answer: {
        groupId
      },
    }, {
      Authorization: `Bearer ${await verifyToken()}`
    })
  }

  const onClickReserve = async () => {
    // initiate login step, phone will be provided from button
    await requestSubscribedMessages()

    setIsLoading(true)
    let reservationIds
    try {
      const token = await verifyToken()

      // reserve the spot immediately for all sub classes
      reservationIds = await Promise.all(
        activeClassGroupData.learning_classes
          .filter(({ id }) => !!unpaidClasses[id])
          .map(async ({ id }) => {
            const { id: reservationId } = await requestPost(`/reservations`, {
              'learning_class': id,
            }, {
              Authorization: `Bearer ${token}`
            })
            return reservationId
          })
      )

      if (!(await payWithPoints({
        userInventory,
        finalCost: classCost,
        token,
        reservationIds,
        description: activeClassGroupData?.name,
        couponId: selectedCoupon?.coupon?.id
      }))) return

      // payment complete
      requestPost(`/member-reward-points-logs`, {
        pointId: config.app.pointsId.first_time_buy_class_group,
      }, {
        Authorization: `Bearer ${token}`
      })
      requestPost(`/member-reward-points-logs`, {
        pointId: config.app.pointsId.buy_class,
        times: reservationIds.length
      }, {
        Authorization: `Bearer ${token}`
      })

      setIsLoading(false)
      await redirectTo({
        url: `/pages/paymentCompletePage/index?reservationId=${reservationIds[0]}`
      })
    } catch (e) {
      console.error(e)
      handleErrorAsJson(e, {
        ALREADY_PRESENT: () => {
          requestWxShowToast({
            title: '已曾购买',
            icon: 'error',
            duration: 1000
          })
        },
        NO_MORE_SPACE: () => {
          requestWxShowToast({
            title: '全部售空',
            icon: 'error',
            duration: 1000
          })
        },
        TIME_NOT_ALLOWED: () => {
          requestWxShowToast({
            title: '已停止预约',
            icon: 'error',
            duration: 1000
          })
        }
      })
    } finally {
      setIsLoading(false)
    }
  }

  const isStoppedReservations = (activeClassGroupData?.learning_classes
    ?.filter(({ id }) => !!unpaidClasses[id])
    ?.map(({ starting_datetime }) => new Date(starting_datetime))
    ?.sort((a, b) => a > b ? -1 : a < b ? 1 : 0)[0] || new Date()) < new Date()
  return (
    <View className={styles.root}>
      <View className={styles.header}>
        {
          activeClassGroupData?.cover_media?.mime?.includes('image') ?
            <Image src={requestStatic(activeClassGroupData.cover_media.url)} className={styles.headerMedia}/>
            : null
        }
        {
          activeClassGroupData?.cover_media?.mime?.includes('video') ?
            <CustomVideo src={requestStatic(activeClassGroupData.cover_media.url)}/>
            : null
        }
        <View className={styles.headerBackButton}
              onClick={() => wx.redirectTo({ url: '/pages/placePage/index?tab=2' })}>
          <Image src={extendedArrowLeftIcon} className={styles.headerBackButtonIcon}/>
          Back
        </View>
      </View>
      <View className={styles.body}>
        <View className={styles.titleRow}>
          {activeClassGroupData?.name}
        </View>
        <View className={styles.tagsRow}>
          {activeClassGroupData?.tags?.split(',').map(x => x.trim()).join('・')}
        </View>
        <Card className={styles.teacherProfileCard}>
          <View className={styles.teacherProfileCardLeft}>
            <ProfilePicture src={activeClassGroupData?.teacher?.profile_picture?.url}/>
          </View>
          <View className={styles.teacherProfileCardRight}>
            <View className={styles.teacherProfileCardRightName}>
              {activeClassGroupData?.teacher?.username}
            </View>
            <View
              className={`${styles.teacherProfileCardRightBackground} ${expandTeacherBackground ? styles.teacherProfileCardRightBackgroundExpandActive : ''}`}>
              {activeClassGroupData?.teacher?.background}
            </View>
            <View
              className={styles.teacherProfileCardRightBackgroundExpand}
              onClick={() => setExpandTeacherBackground(!expandTeacherBackground)}>
              <Image src={arrowDownIcon}
                     className={`${styles.teacherProfileCardRightBackgroundExpandIcon} ${expandTeacherBackground ? styles.teacherProfileCardRightBackgroundExpandIconExpandActive : ''}`}/>
            </View>
          </View>
        </Card>
        <Card className={styles.classDetailsCard}>
          <View className={styles.classDetailsCardEntry}>
            <View className={styles.classDetailsCardLabel}>
              {/*<Image src={shirtIcon} className={styles.classDetailsCardLabelIcon}/>*/}
              课前准备
            </View>
            <Text className={styles.classDetailsCardText}>
              {activeClassGroupData?.keqianzhunbei_description}
            </Text>
          </View>
          <View className={styles.classDetailsCardEntry}>
            <View className={styles.classDetailsCardLabel}>
              {/*<Image src={clockIcon} className={styles.classDetailsCardLabelIcon}/>*/}
              上课时间
            </View>
            <View className={styles.classDetailsCardContent}>
              {
                Object.values(unpaidClasses).filter(x => !x).length ? <View
                  className={`${styles.classDetailsCardText} ${styles.classDetailsCardTextDate}`}>
                  已购{Object.values(unpaidClasses).filter(x => !x).length}节课
                </View> : null
              }
              {activeClassGroupData?.starting_datetime ?
                activeClassGroupData?.learning_classes
                  .filter(({ id }) => !!unpaidClasses[id])
                  .sort((a, b) => {
                    return new Date(a.starting_datetime) - new Date(b.starting_datetime)
                  })
                  .map(({ starting_datetime, ending_datetime }) => {
                    const startingDatetime = new Date(starting_datetime)
                    const endingDatetime = new Date(ending_datetime)
                    return <View className={`${styles.classDetailsCardText} ${styles.classDetailsCardTextDate}`}>
                      {(startingDatetime).getMonth() + 1}.{startingDatetime.getDate()}(周{chineseDays[startingDatetime.getDay()]}) {startingDatetime.getHours()}:{formatTimeElement(startingDatetime.getMinutes())}-{endingDatetime.getHours()}:{formatTimeElement(endingDatetime.getMinutes())}
                    </View>
                  })
                : null}
            </View>
          </View>
          <View className={styles.classDetailsCardEntry}>
            <View className={styles.classDetailsCardLabel}>
              {/*<Image src={mapHollowIcon} className={styles.classDetailsCardLabelIcon}/>*/}
              上课地点
            </View>
            <Text className={styles.classDetailsCardText}>
              {publicSpace?.full_address}
            </Text>
          </View>
          <View className={styles.classDetailsCardViewMap} onClick={() => wx.openLocation({
            latitude: +publicSpace?.latitude,
            longitude: +publicSpace?.longitude,
            name: publicSpace?.name,
            address: publicSpace?.name,
          })}>
            查看地图
          </View>
        </Card>

        <PaymentCard
          originalPrice={totalOriginalPrice}
          cost={classCost}
          setIsLoading={setIsLoading}
          selectedCoupon={selectedCoupon}
          setSelectedCoupon={setSelectedCoupon}
          hours={activeClassGroupData?.learning_classes?.length}
          caloriesBurnRate={activeClassGroupData?.calorie_burn_rate}
          couponTypeFilter={'place'}
        />
        <Card className={styles.noticeCard}>
          <View className={`${styles.noticeCardTitle}`}>
            注意事项
          </View>
          <View className={`${styles.noticeCardText}`}>
            1. 约课后请提前15分钟到达门店，用“我的预约”中二维码扫码开门签到；
          </View>
          <View className={`${styles.noticeCardText}`}>
            2. 门店提供更衣室，不设淋浴；
          </View>
          <View className={`${styles.noticeCardText}`}>
            3. 门店内设高速无线网络。WIFI密码:p7community；
          </View>
          <View className={`${styles.noticeCardText}`}>
            4. 门店内设直饮净水，可自行携带水杯或水壶；
          </View>
          <View className={`${styles.noticeCardText}`}>
            5. 课程开始前6小时内取消预约，不支持退款；
          </View>
          <View className={`${styles.noticeCardText}`}>
            6. 课程开始前2小时，报名不满3人，系统自动通知老师和学员课程取消，费用退至钱包。
          </View>
          <View className={`${styles.noticeCardText}`}>
            7. 成功预约本课程即表示已阅读并同意 <Text className={styles.link}
                                      onClick={() => navigateTo({ url: '/pages/privacyStatementPage/index' })}>《P7
            COMMUNITY舞蹈健身公社隐私协议》</Text>及<Text className={styles.link}
                                              onClick={() => navigateTo({ url: '/pages/thingsToNotePage/index' })}>《P7
            COMMUNITY舞蹈健身公社注意事项及免责声明》</Text>。
          </View>
        </Card>
        <View className={styles.bodyPadding}/>
      </View>
      <View className={styles.footer}>
        {
          isStoppedReservations ?
            <View className={styles.actionButtonReserve} onClick={onClickSubscribe}>
              已结束，立即催更
            </View> :
            <View className={styles.actionButtonReserve} onClick={onClickReserve}>
              立即预约
            </View>
        }
      </View>
      <LoadingScreen loading={isLoading}/>
    </View>
  )
};
