import * as React from 'react'
import { useEffect, useMemo, useState } from 'react'
import { Image, Text, View } from 'remax/wechat'
import styles from './index.css'
import crossIcon from '../../assets/icons/cross-white.svg'

import { useQuery } from 'remax'
import { requestGet, requestPost, requestWxShowToast } from '../../library/wxPromise'
import LoadingScreen from '../../components/loading'
import Card from '../../components/card'
import { useShareAppMessage } from '../../library/hooks'
import { useDb } from '../../library/db'
import { chineseDays, dbCollections } from '../../library/constants'
import DayInWeekSelector from '../../components/dayInWeekSelector'
import EmptySpace from '../../components/emptySpace'
import QnA from '../../components/qna'
import { config } from '../../library/config'
import { getUserData, verifyToken } from '../../library/user'
import extendedArrowLeftIcon from '../../assets/icons/extendedarrowleft.svg'

export default () => {
  const [openingHours, setDefaultOpeningHours] = useDb(dbCollections.openingHours)
  const [selectedOpeningHours, setDefaultSelectedOpeningHours] = useDb(dbCollections.teacherSelectedOpeningHours)
  const [formQuestions, setDefaultFormQuestions] = useDb(dbCollections.formQuestions)
  const [userFormAnswers, setDefaultUserFormAnswers] = useDb(dbCollections.userFormAnswers)

  const [inactiveOpeningHours, setInactiveOpeningHours] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [currentDay, setCurrentDay] = useState(0)
  const [selectedDayOpeningHours, setSelectedDayOpeningHours] = useState({})
  const [questionOneAnswer, setQuestionOneAnswer] = useState(null)
  const [questionTwoAnswer, setQuestionTwoAnswer] = useState(null)
  const [questionThreeAnswer, setQuestionThreeAnswer] = useState([])
  const [selectedDayOpeningHoursId, setSelectedDayOpeningHoursId] = useState({})
  const [questionOneId, setQuestionOneId] = useState(null)
  const [questionTwoId, setQuestionTwoId] = useState(null)
  const [questionThreeId, setQuestionThreeId] = useState(null)

  let { id: publicSpaceId, date } = useQuery()

  useEffect(() => {
    const today = new Date()

    async function _ () {
      setIsLoading(true)
      const token = await verifyToken()
      const user = await getUserData(token)
      const [, selectedOpeningHours, , userFormAnswers] = await Promise.all([
        setDefaultOpeningHours(() => requestGet(`/opening-hours`)),
        setDefaultSelectedOpeningHours(() => requestGet(`/teacher-selected-opening-hours`)),
        setDefaultFormQuestions(() => requestGet(`/form-questions`)),
        setDefaultUserFormAnswers(() => requestGet(`/user-form-answers`, {
          Authorization: `Bearer ${token}`
        }))
      ])
      const selectedOpeningHoursForThisMonth = selectedOpeningHours.find(({ month, year }) => {
        // we are doing it for the next month
        return month === today.getMonth() + 2 && year === today.getFullYear()
      })
      const selfSelectedOpeningHours = selectedOpeningHoursForThisMonth.filter(({ teacher }) => {
        // we are doing it for the next month
        return teacher.id === user.id
      })
      if (selfSelectedOpeningHours?.length) {
        setSelectedDayOpeningHoursId(selfSelectedOpeningHours.reduce((acc, ele) => {
          acc[ele.day] = ele.id
          return acc
        }, {}))
        setSelectedDayOpeningHours(selfSelectedOpeningHours.reduce((acc, ele) => {
          acc[ele.day] = ele.opening_hours.map(({ id }) => id)
          return acc
        }, {}))
      }
      // setInactiveOpeningHours(selectedOpeningHoursForThisMonth.filter(({ teacher }) => {
      //   // we are doing it for the next month
      //   return teacher.id !== user.id
      // }).reduce((acc, ele) => {
      //   acc[ele.day] = ele.opening_hours.map(({ id }) => id)
      //   return acc
      // }, {}))
      const questionOneAnswer = userFormAnswers.findOne(({ form_question, user: answeredUser }) => {
        return form_question.id === config.app.formQuestions.teacherQuestions[0] && user.id === answeredUser.id
      })
      if (questionOneAnswer?.form_answers?.length) {
        setQuestionOneAnswer(questionOneAnswer.form_answers[0].id)
        setQuestionOneId(questionOneAnswer.form_question.id)
      }
      const questionTwoAnswer = userFormAnswers.findOne(({ form_question, user: answeredUser }) => {
        return form_question.id === config.app.formQuestions.teacherQuestions[1] && user.id === answeredUser.id
      })
      if (questionTwoAnswer?.form_answers?.length) {
        setQuestionTwoAnswer(questionTwoAnswer.form_answers[0].id)
        setQuestionTwoId(questionTwoAnswer.form_question.id)
      }

      const questionThreeAnswer = userFormAnswers.findOne(({ form_question, user: answeredUser }) => {
        return form_question.id === config.app.formQuestions.teacherQuestions[2] && user.id === answeredUser.id
      })
      if (questionThreeAnswer?.form_answers?.length) {
        setQuestionThreeAnswer(questionThreeAnswer.form_answers.map(({ id }) => id))
        setQuestionThreeId(questionThreeAnswer.form_question.id)
      }
      setIsLoading(false)
    }

    _()
  }, [publicSpaceId])

  const onSubmit = async () => {
    try {
      if (!questionOneAnswer || !questionTwoAnswer || !questionThreeAnswer?.length) {
        return requestWxShowToast({
          title: '仍未填完',
          icon: 'error',
          duration: 1000
        })
      }
      if (!Object.values(selectedDayOpeningHours).flat().length) {
        return requestWxShowToast({
          title: '未选择时间',
          icon: 'error',
          duration: 1000
        })
      }
      const token = await verifyToken()
      const today = new Date()
      const [] = await Promise.all([
        Promise.all(Object.entries(selectedDayOpeningHours).map(([day, hourIds]) => {
          return requestPost(`/teacher-selected-opening-hours${selectedDayOpeningHoursId[day] ? `/${selectedDayOpeningHoursId[day]}` : ''}`, {
            opening_hours: hourIds,
            day: day,
            month: today.getMonth() + 2,
            year: today.getFullYear(),
          }, {
            Authorization: `Bearer ${token}`
          })
        })),
        Promise.all([
          requestPost(`/user-form-answers${questionOneId ? `/${questionOneId}` : ''}`, {
            form_question: config.app.formQuestions.teacherQuestions[0],
            form_answers: [questionOneAnswer],
          }, {
            Authorization: `Bearer ${token}`
          }),
          requestPost(`/user-form-answers${questionTwoId ? `/${questionTwoId}` : ''}`, {
            form_question: config.app.formQuestions.teacherQuestions[1],
            form_answers: [questionTwoAnswer],
          }, {
            Authorization: `Bearer ${token}`
          }),
          requestPost(`/user-form-answers${questionThreeId ? `/${questionThreeId}` : ''}`, {
            form_question: config.app.formQuestions.teacherQuestions[2],
            form_answers: questionThreeAnswer,
          }, {
            Authorization: `Bearer ${token}`
          })
        ])
      ])
      return requestWxShowToast({
        title: '提交完成',
        icon: 'success',
        duration: 1000
      })
    } catch (e) {

    }
  }

  useShareAppMessage(setIsLoading)

  const onClickHour = (openHoursId, day) => {
    let targetDay = currentDay
    if (day) {
      targetDay = day
    }
    if (selectedDayOpeningHours[targetDay]?.find(i => i === openHoursId)) {
      setSelectedDayOpeningHours({
        ...selectedDayOpeningHours,
        [targetDay]: selectedDayOpeningHours[targetDay].filter(i => i !== openHoursId)
      })
    } else {
      setSelectedDayOpeningHours({
        ...selectedDayOpeningHours,
        [targetDay]: [...(selectedDayOpeningHours[targetDay] || []), openHoursId]
      })
    }
  }
  const totalOpeningHours = useMemo(() => Object.entries(selectedDayOpeningHours).map(([day, hourIds]) => hourIds.map(hourId => [day, hourId])).flat(), [selectedDayOpeningHours])

  return (
    <View className={styles.root}>
      <View className={styles.header}>
      <View className={styles.headerBackButton}
            onClick={() => wx.redirectTo({ url: `/pages/teacherCalendarPage/index` })}>
        <Image src={extendedArrowLeftIcon} className={styles.headerBackButtonIcon}/>
        Back
      </View>
      </View>
      <EmptySpace vertical={8} scaleToHeight={true}/>
      <View className={styles.body}>
        <DayInWeekSelector
          title={`${(new Date()).getMonth() + 2}月排课`}
          currentDay={currentDay}
          setCurrentDay={setCurrentDay}
        />
        <Card className={styles.openingHoursSelectorRoot}>
          <View className={styles.openingHoursSelectorHeader}>
            <View className={styles.openingHoursSelectorHeaderElement}>
              上午
            </View>
            <View className={styles.openingHoursSelectorHeaderElement}>
              下午
            </View>
            <View className={styles.openingHoursSelectorHeaderElement}>
              晚上
            </View>
          </View>
          <View className={styles.openingHoursSelector}>
            {
              openingHours.find()?.map(({ starting_time, ending_time, id: openHoursId }) => {
                let [startingTimeHours, startingTimeMinutes] = starting_time.split(':')
                let [endingTimeHours, endingTimeMinutes] = ending_time.split(':')
                let isInactive = inactiveOpeningHours[currentDay]?.find(id => id === openHoursId)
                return <View
                  className={`${styles.openingHoursSelectorElement} 
                  ${isInactive ? styles.openingHoursSelectorElementInactive : ''} 
                  ${selectedDayOpeningHours[currentDay]?.find(id => id === openHoursId) ? styles.openingHoursSelectorElementSelected : ''}`}
                  onClick={() => onClickHour(openHoursId)}
                >
                  <View className={styles.openingHoursSelectorElementTop}>
                    {startingTimeHours}:{startingTimeMinutes}-{endingTimeHours}:{endingTimeMinutes}
                  </View>
                </View>
              })
            }
          </View>
        </Card>
        <Card className={styles.classDetailsCard}>
          <View className={styles.classDetailsCardEntry}>
            <View className={styles.classDetailsCardLabel}>
              选择时间
            </View>
            <View className={styles.classDetailsCardContent}>
              {
                totalOpeningHours.map(([day, openingHourId]) => {
                  const hour = openingHours.findOne(openingHourId)
                  const [startingHour, startingMinutes] = hour.starting_time.split(':')
                  const [endingHour, endingMinutes] = hour.ending_time.split(':')
                  return <View
                    className={styles.classDetailsCardContentEntry}
                    onClick={() => onClickHour(openingHourId, day)}

                  >
                    <View>
                      <Text>
                        周{chineseDays[day]} {startingHour}:{startingMinutes}-{endingHour}:{endingMinutes}
                      </Text>
                    </View>
                    <View className={styles.classDetailsCardContentEntryIconRoot}>
                      <Image className={styles.classDetailsCardContentEntryIcon} src={crossIcon}/>
                    </View>
                  </View>
                })
              }
            </View>
          </View>
        </Card>
        <Card className={styles.classDetailsCard}>
          <View className={styles.classDetailsCardEntry}>
            <View className={styles.classDetailsCardLabel}>
              排课要求
            </View>
          </View>
          <View className={styles.classDetailsCardContent}>
            {
              (() => {
                const question = formQuestions.findOne(config.app.formQuestions.teacherQuestions[0])
                return <QnA
                  question={question.content}
                  selection={question.form_answers}
                  selectedAnswers={[questionOneAnswer]}
                  onSelectedAnswer={id => setQuestionOneAnswer(id)}
                />
              })()
            }
            {
              (() => {
                const question = formQuestions.findOne(config.app.formQuestions.teacherQuestions[1])
                return <QnA
                  question={question.content}
                  selection={question.form_answers}
                  selectedAnswers={[questionTwoAnswer]}
                  onSelectedAnswer={id => setQuestionTwoAnswer(id)}
                />
              })()
            }
            {
              (() => {
                const question = formQuestions.findOne(config.app.formQuestions.teacherQuestions[2])
                return <QnA
                  question={question.content}
                  selection={question.form_answers}
                  selectedAnswers={questionThreeAnswer}
                  onSelectedAnswer={id => {
                    if (questionThreeAnswer.includes(id)) {
                      setQuestionThreeAnswer(questionThreeAnswer.filter(x => x !== id))
                    } else {
                      setQuestionThreeAnswer([...questionThreeAnswer, id])
                    }
                  }}
                />
              })()
            }
          </View>
        </Card>
        <Card className={styles.noticeCard}>
          <View className={`${styles.noticeCardTitle}`}>
            注意事项
          </View>
          <View className={`${styles.noticeCardText}`}>
            1. 课程安排好，提前1个半小时，在“老师入口”中确认当日能授课，提前15分钟进场扫码签到入场，离开时扫码签退。
          </View>
          <View className={`${styles.noticeCardText}`}>
            2. 临时无法来上课，请提前一天通知工作人员。
          </View>
          <View className={`${styles.noticeCardText}`}>
            3. 门店提供更衣室，不设淋浴。
          </View>
          <View className={`${styles.noticeCardText}`}>
            4. 门店设沉浸式七彩动感灯光系统，如需开设，请联系客服。
          </View>
          <View className={`${styles.noticeCardText}`}>
            5. 门店音响设备设蓝牙，按第四个键连接设备蓝牙”BT-AMPLIFIER”。
          </View>
          <View className={`${styles.noticeCardText}`}>
            6. 门店提供多种音响转接口。
          </View>
          <View className={`${styles.noticeCardText}`}>
            7. 门店内设直饮纯净水，可自行携带水杯和水壶。
          </View>
          <View className={`${styles.noticeCardText}`}>
            8. 门店设自动跟拍设备，可自行根据需要使用 。
          </View>
          <View className={`${styles.noticeCardText}`}>
            9. 门店设高速无线网络。WIFI密码：p7community。
          </View>
          <View className={`${styles.noticeCardText}`}>
            10. 用完设备和器具，请将设备和器具归回原位，保持场地干净整洁。
          </View>
          <View className={`${styles.noticeCardText}`}>
            11. 救急老师需要在接到救急安排后，在课程开始前一个小时以内赶到教室授课。
          </View>
          <View className={`${styles.noticeCardText}`}>
            12. 所有课酬在次月8日前核对，10日发放上月的课酬。
          </View>
          <View className={`${styles.noticeCardText}`}>
            12. 如门店操作中遇到任何问题，请与服务你的工作人员联系。
          </View>
          <View className={`${styles.noticeCardText}`}>
            13. 课程结束老师须负责拍摄视频，并在课程结束后第一时间发送给客服，由客服统一录入学生后台，并发送给学生。
          </View>
        </Card>
        <View className={styles.bodyPadding}/>
      </View>
      <View className={styles.footer}>
        {
          <View className={styles.actionButtonReserve} onClick={onSubmit}>
            提交
          </View>
        }

      </View>
      <LoadingScreen loading={isLoading}/>
    </View>
  )
};
