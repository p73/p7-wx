import * as React from 'react'
import { useEffect, useState } from 'react'
import { Button, Image, redirectTo, Text, View } from 'remax/wechat'
import { useQuery } from 'remax'
import styles from './index.css'
import Tab from '../../components/tab'
import Card from '../../components/card'
import ProfilePicture from '../../components/profilePicture'
import PlacePicture from '../../components/placePicture'
import GroupPicture from '../../components/groupPicture'
import LoadingScreen from '../../components/loading'
import DaySelector from '../../components/daySelector'
import { addDays, formatTimeElement } from '../../library/date'
import arrowRightIcon from '../../assets/icons/arrowright.svg'
import { requestGet, requestWxShowToast } from '../../library/wxPromise'
import Header from '../../components/header'
import { bindPhone, verifyToken } from '../../library/user'
import { useShareAppMessage } from '../../library/hooks'
import { couponDataConstants, dbCollections } from '../../library/constants'
import { useDb } from '../../library/db'
import LocationSelector from '../../components/locationSelector'
import { placeSelectionBuilderSelectedDefault } from '../../library/places'
import { config } from '../../library/config'
import { handleErrorLogging } from '../../library/error'

export default () => {
  const { publicSpaceId, toDate, tab } = useQuery()
  const [isUserLoggedIn, setIsUserLoggedIn] = useState(false)
  const [activeTab, setActiveTab] = useState(0)
  const [currentDay, setCurrentDay] = useState()
  const [learningClasses, setDefaultLearningClasses] = useDb(dbCollections.learningClasses)
  const [learningClassGroups, setDefaultLearningClassGroups] = useDb(dbCollections.learningClassGroups)
  const [publicSpaces, setDefaultPublicSpaces] = useDb(dbCollections.publicSpaces)
  const [isLoading, setIsLoading] = useState(false)
  const [couponData, setDefaultCouponData] = useDb(dbCollections.couponDatas)
  const [openingHours, setDefaultOpeningHours] = useDb(dbCollections.openingHours)
  const [reservations, setDefaultReservations] = useDb(dbCollections.reservations)
  const [roomDatas, setDefaultRoomDatas] = useDb(dbCollections.roomDatas)
  const [spaceOpeningHours, setSpaceOpeningHours] = useState({})

  const [selectedCityFilter, setSelectedCityFilter] = useState(placeSelectionBuilderSelectedDefault)

  const tabs = [
    {
      name: '课程预约',
      id: 0
    },
    {
      name: '成品舞',
      id: 2
    },
    {
      name: '舞场租赁',
      id: 1
    }
  ]

  useEffect(() => {
    if (isNaN(+tab)) return
    setActiveTab(+tab)
  }, [tab])

  useEffect(() => {
    if (toDate) {
      setCurrentDay(new Date(toDate))
    }
  }, [toDate])

  useEffect(() => {
    async function _ () {
      setIsLoading(true)
      try {

        const token = await verifyToken()
        setIsUserLoggedIn(!!wx.getStorageSync(config.storageKey.collectedPhone))
        await setDefaultPublicSpaces(() => requestGet(`/public-spaces`))
        await setDefaultLearningClasses(() => requestGet(`/learning-classes`))
        await setDefaultLearningClassGroups(() => requestGet(`/learning-classes/groups`))
        await setDefaultCouponData(() => requestGet(`/coupon-data/1`))
        await setDefaultOpeningHours(() => requestGet(`/opening-hours`))
        await setDefaultReservations(() => requestGet(`/reservations`, {
          Authorization: `Bearer ${token}`
        }), true)
        await setDefaultPublicSpaces(() => requestGet(`/public-spaces`))
        await setDefaultRoomDatas(() => requestGet(`/room-data`))
      } catch (e) {
        handleErrorLogging(e)
      }

      setIsLoading(false)
    }

    _()
  }, [publicSpaceId])

  useEffect(() => {
    const today = new Date()
    today.setHours(0)
    today.setMinutes(0)
    today.setSeconds(0)
    today.setMilliseconds(0)
    if (publicSpaceId) {
      const { city } = publicSpaces.findOne(publicSpaceId)
      setSelectedCityFilter({
        id: city,
        name: city
      })
    }

    // find rooms opening hours count available in 3 days
    // we are only interested in the count
    const tempSpaceOpeningHours = {}
    for (const { id: spaceId } of publicSpaces.find()) {
      let temp = {}
      for (let i = 0; i < 3; i++) {
        let currentDay = addDays(today, i)
        // remove all opening hours that are the same as any learning classes
        let classForTheDayCount = learningClasses.find(({ starting_datetime, public_space }) => {
          if (spaceId !== public_space?.id) {
            return false
          }
          const startingDatetime = new Date(starting_datetime)
          return startingDatetime.getDate() === currentDay.getDate() && startingDatetime.getMonth() === currentDay.getMonth() && startingDatetime.getFullYear() === currentDay.getFullYear()
        })
        let reservationsForTheDay = reservations.find(({ starting_datetime, learning_class, public_space, status }) => {
          if (status === 'Cancelled') {
            return false
          }
          // only filter them for the right space
          if (learning_class) {
            if (spaceId !== learning_class.public_space) {
              return false
            }
          }

          if (public_space) {
            if (spaceId !== public_space) {
              return false
            }
          }
          const startingDatetime = new Date(starting_datetime || learning_class.starting_datetime)
          return startingDatetime.getDate() === currentDay.getDate() && startingDatetime.getMonth() === currentDay.getMonth() && startingDatetime.getFullYear() === currentDay.getFullYear()
        })
        // remove all reservations within a certain time
        temp[currentDay.toISOString()] = openingHours.find().length - classForTheDayCount.length - reservationsForTheDay.length
      }
      tempSpaceOpeningHours[spaceId] = temp
    }
    setSpaceOpeningHours({
      ...spaceOpeningHours,
      ...tempSpaceOpeningHours
    })
  }, [publicSpaceId, publicSpaces, reservations, openingHours, learningClasses])

  useShareAppMessage(setIsLoading)

  const onClickChooseCourse = async (classId) => {
    await redirectTo({
      url: `/pages/classDetailsPage/index?id=${classId}&toDate=${currentDay.toISOString()}`
    })
  }

  const onClickChooseCourseGroup = async (groupId) => {
    await redirectTo({
      url: `/pages/classGroupDetailsPage/index?id=${groupId}`
    })
  }

  const onClickChooseSpace = async (publicSpaceId, date) => {
    await redirectTo({
      url: `/pages/spaceDetailsPage/index?id=${publicSpaceId}&date=${date}`
    })
  }

  const onBindPhone = async phone => {
    if (!phone?.detail?.iv) {
      // error out, don't allow
      requestWxShowToast({
        title: '必须绑定手机',
        icon: 'error',
        duration: 1000
      })
      return false
    }
    setIsLoading(true)
    await bindPhone(phone.detail)
    setIsLoading(false)
    return true
  }

  return <View className={styles.root}>
    <Header title={'P7 COMMUNITY'}>
      <LocationSelector
        publicSpacesList={publicSpaces.find()}
        selectedCityFilter={selectedCityFilter}
        setSelectedCityFilter={setSelectedCityFilter}
      />
      <View className={styles.headerTab}>
        {
          tabs.map(({ name, id }) => {
            return <View
              className={`${styles.headerTabElement} ${id === activeTab ? styles.headerTabElementActive : ''}`}
              onClick={() => setActiveTab(id)}
            >
              <Text className={styles.headerTabElementText}>
                {name}
              </Text>
            </View>
          })
        }
      </View>
    </Header>
    {
      (() => {
        switch (activeTab) {
          case 0:
            return <DaySelector
              currentDay={currentDay}
              setCurrentDay={setCurrentDay}
            />
          default:
            return null
        }
      })()
    }

    <View className={styles.banner}>
      <Text className={styles.bannerMockText}>
        这是一个Banner
      </Text>
    </View>
    <View className={styles.body}>
      {
        (() => {
          switch (activeTab) {
            case 0:
              return publicSpaces.find(({ city }) => {
                if (selectedCityFilter.id) {
                  return city === selectedCityFilter.id
                }
                return true
              })?.map(({ latitude, longitude, address, name, id: publicSpaceId }) => {
                const activeClasses = learningClasses.find(({ id, public_space, starting_datetime }) => {
                  if (!public_space?.id) {
                    return false
                  }
                  if (public_space.id !== publicSpaceId) {
                    return false
                  }
                  if (!currentDay) {
                    return false
                  }
                  if (learningClassGroups.findOne(id)) {
                    return false
                  }
                  let startingDatetime = new Date(starting_datetime)
                  return startingDatetime.getDate() === currentDay.getDate() && startingDatetime.getMonth() === currentDay.getMonth()
                })
                if (!activeClasses.length) return null
                return <View className={styles.card} key={publicSpaceId}>
                  <View className={styles.cardHeader}>
                    <View className={styles.cardTitleLeft}>
                      {name}
                    </View>
                    <View className={styles.cardTitleRight}>
                      {address}
                      <View className={styles.cardTitleRightViewMap} onClick={e => {
                        e.stopPropagation()
                        wx.openLocation({
                          latitude: +latitude,
                          longitude: +longitude,
                          name: name,
                          address: name,
                        })
                      }}>
                        查看地图
                      </View>
                    </View>
                  </View>
                  <View className={styles.cardBody}>
                    {
                      activeClasses.sort((a, b) => {
                        return new Date(a.starting_datetime) - new Date(b.starting_datetime)
                      }).map(({
                        id: learningClassId,
                        teacher,
                        starting_datetime,
                        ending_datetime,
                        tags,
                        price_data,
                        name,
                      }) => {
                        const startingDateTime = new Date(starting_datetime)
                        const endingDateTime = new Date(ending_datetime)
                        return <Card key={learningClassId}>
                          {isUserLoggedIn ? <View className={styles.innerCardOverlay}
                                                  onClick={async () => {
                                                    await onClickChooseCourse(learningClassId)
                                                  }}>
                          </View> : <Button className={styles.innerCardOverlay} openType="getPhoneNumber"
                                            onGetPhoneNumber={async phone => {
                                              if (await onBindPhone(phone)) {
                                                await onClickChooseCourse(learningClassId)
                                              }
                                            }}>
                          </Button>}
                          <View className={styles.innerCardHeader}>
                            <View className={styles.innerCardHeaderLeft}>
                              <View className={styles.innerCardHeaderLeftDate}>
                                {startingDateTime.getMonth() + 1}.{startingDateTime.getDate()}
                              </View>
                              <View className={styles.innerCardHeaderLeftTime}>
                                {startingDateTime.getHours()}:{formatTimeElement(startingDateTime.getMinutes())}-{endingDateTime.getHours()}:{formatTimeElement(endingDateTime.getMinutes())}
                              </View>
                            </View>
                            <View className={styles.innerCardHeaderRight}>
                              预约
                              <Image src={arrowRightIcon} className={styles.innerCardHeaderRightIcon}/>
                            </View>
                          </View>
                          <View className={styles.innerCardBody}>
                            <View className={styles.innerCardBodyLeft}>
                              <ProfilePicture src={teacher?.profile_picture?.url}/>
                            </View>
                            <View className={styles.innerCardBodyRight}>
                              <View className={styles.innerCardBodyRightHeader}>
                                <View className={styles.innerCardBodyRightHeaderLeft}>
                                  {name}
                                </View>
                                <View className={styles.innerCardBodyRightHeaderRight}>
                                  {teacher?.username}
                                </View>
                              </View>
                              <View className={styles.innerCardBodyRightTags}>
                                {tags.split(',').map(x => x.trim()).join('・')}
                              </View>
                              <View className={styles.innerCardBodyRightPrice}>
                                ¥{price_data.original_price} |
                                VIP¥{Math.round(price_data.original_price * config.app.memberDiscount)} |
                                新会员 <Text
                                className={styles.innerCardBodyRightPriceNewMemberNumber}>¥{(price_data.original_price - couponData.findOne(couponDataConstants.defaultNewUserCouponData)?.flat_discount).toFixed(0)}</Text>
                              </View>
                            </View>
                          </View>
                        </Card>

                      })
                    }
                  </View>
                </View>

              })
            case 1:
              // return <View style={{
              //   width: '100%',
              //   textAlign: 'center'
              // }}>
              //   还在构建中，敬请期待
              // </View>
              return publicSpaces.find(({ city, is_ready }) => {
                if (!is_ready) return false
                if (selectedCityFilter.id) {
                  return city === selectedCityFilter.id
                }
                return true
              })?.map(({ address, name, id: spaceId }) => {
                return <View className={styles.card} key={spaceId}>
                  <View className={styles.cardHeader}>
                    <View className={styles.cardTitleLeft}>
                      {name}
                    </View>
                    <View className={styles.cardTitleRight}>
                      {address}
                      <View className={styles.cardTitleRightViewMap} onClick={e => {
                        e.stopPropagation()
                        wx.openLocation({
                          latitude: +latitude,
                          longitude: +longitude,
                          name: name,
                          address: name,
                        })
                      }}>
                        查看地图
                      </View>
                    </View>
                  </View>
                  <View className={styles.cardBody}>
                    {
                      (() => {
                        const openingHoursLeft = spaceOpeningHours[spaceId]
                        const { cover_media, room } = publicSpaces.findOne(spaceId)
                        const { price_data } = roomDatas.findOne(room.id)
                        // noinspection JSCheckFunctionSignatures
                        return Object.entries(openingHoursLeft).map(([dateString, slots]) => {
                          let date = new Date(dateString)
                          return <Card key={spaceId + dateString + slots}>
                            {isUserLoggedIn ? <View className={styles.innerCardOverlay}
                                                    onClick={async () => await onClickChooseSpace(spaceId, dateString)}>
                            </View> : <Button className={styles.innerCardOverlay} openType="getPhoneNumber"
                                              onGetPhoneNumber={async phone => {
                                                if (await onBindPhone(phone)) {
                                                  await onClickChooseSpace(spaceId, dateString)
                                                }
                                              }}>
                            </Button>}
                            <View className={styles.innerCardHeader}>
                              <View className={styles.innerCardHeaderLeft}>
                                <View className={styles.innerCardHeaderLeftDate}>
                                  {date.getMonth() + 1}.{date.getDate()}
                                </View>
                                <View className={styles.innerCardHeaderLeftTime}>
                                  还有{slots}个时段可预约
                                </View>
                              </View>
                              <View className={styles.innerCardHeaderRight}>
                                预约
                                <Image src={arrowRightIcon} className={styles.innerCardHeaderRightIcon}/>
                              </View>
                            </View>
                            <View className={styles.innerCardBody}>
                              <View className={styles.innerCardBodyLeft}>
                                <PlacePicture src={cover_media?.formats.small.url}/>
                              </View>
                              <View className={styles.innerCardBodyRight}>
                                <View className={styles.innerCardBodyRightHeader}>
                                  <View className={styles.innerCardBodyRightHeaderLeft}>
                                    {room?.type}
                                  </View>
                                </View>
                                <View className={styles.innerCardBodyRightTags}>
                                  最多可容纳{room?.max_supported_person}人
                                </View>
                                <View className={styles.innerCardBodyRightPrice}>
                                  ¥{price_data?.original_price} |
                                  VIP¥{Math.round(price_data?.original_price * config.app.memberDiscount)} |
                                  新会员 <Text
                                  className={styles.innerCardBodyRightPriceNewMemberNumber}>¥{(price_data?.original_price - couponData.findOne(couponDataConstants.defaultNewUserCouponData)?.flat_discount).toFixed(0)}</Text>
                                </View>
                              </View>
                            </View>
                          </Card>
                        })
                      })()
                    }
                  </View>
                </View>
              })

            case 2:
              const today = new Date()
              return <>
                {
                  (() => {
                    const allLearningClassGroups = learningClassGroups
                      .find(({ public_space: { city } }) => {
                        if (selectedCityFilter.id) {
                          return city === selectedCityFilter.id
                        }
                        return true
                      })
                      .map((
                        { learning_classes, ...rest }
                      ) => {
                        learning_classes = learning_classes
                          .map(lclass => ({
                            ...lclass,
                            starting_datetime: new Date(lclass.starting_datetime),
                            ending_datetime: new Date(lclass.ending_datetime),
                          }))
                          .sort((a, b) => a.starting_datetime - b.starting_datetime)
                        return {
                          ...rest,
                          learning_classes,
                          cutoff_datetime: learning_classes[0].starting_datetime
                        }
                      })
                    const upcomingClassGroups = allLearningClassGroups
                      .filter(({ cutoff_datetime }) => cutoff_datetime > today)
                      .sort((a, b) => a.cutoff_datetime - b.cutoff_datetime)
                    const pastClassGroups = allLearningClassGroups
                      .filter(({ cutoff_datetime }) => cutoff_datetime <= today)
                      .sort((a, b) => b.cutoff_datetime - a.cutoff_datetime)

                    const view = groups => groups.map(({
                      name,
                      teacher,
                      tags,
                      id: learningClassGroupId,
                      learning_classes,
                      thumbnail_media
                    }) => {
                      const startingDateTime = learning_classes[0].starting_datetime
                      const endingDateTime = learning_classes[learning_classes.length - 1].starting_datetime
                      const totalOriginalPrice = learning_classes.map(({ price_data }) => price_data.original_price).reduce((a, b) => a + b, 0)
                      return <Card key={learningClassGroupId}>
                        {isUserLoggedIn ?
                          <View className={styles.innerCardOverlay}
                                onClick={async () => {
                                  await onClickChooseCourseGroup(learningClassGroupId)
                                }}/> :
                          <Button
                            className={styles.innerCardOverlay} openType="getPhoneNumber"
                            onGetPhoneNumber={async phone => {
                              if (await onBindPhone(phone)) {
                                await onClickChooseCourseGroup(learningClassGroupId)
                              }
                            }}>
                          </Button>}
                        <View className={styles.innerCardHeader}>
                          <View className={styles.innerCardHeaderLeft}>
                            <View className={styles.innerCardHeaderLeftDate}>
                              {startingDateTime.getMonth() + 1}.{startingDateTime.getDate()} - {endingDateTime.getMonth() + 1}.{endingDateTime.getDate()} | {learning_classes.length}节课
                            </View>
                          </View>
                          <View className={styles.innerCardHeaderRight}>
                            预约
                            <Image src={arrowRightIcon} className={styles.innerCardHeaderRightIcon}/>
                          </View>

                        </View>
                        <View className={styles.innerCardBody}>
                          <View className={styles.innerCardBodyLeft}>
                            <GroupPicture src={thumbnail_media?.url || teacher?.profile_picture?.url}/>
                          </View>
                          <View className={styles.innerCardBodyRight}>
                            <View className={styles.innerCardBodyRightHeader}>
                              <View className={styles.innerCardBodyRightHeaderLeft}>
                                {name}
                              </View>
                              <View className={styles.innerCardBodyRightHeaderRight}>
                                {teacher?.username}
                              </View>
                            </View>
                            <View className={styles.innerCardBodyRightTags}>
                              {tags.split(',').map(x => x.trim()).join('・')}
                            </View>
                            {
                              (endingDateTime > today) ?
                                <View className={styles.innerCardBodyRightPrice}>
                                  ¥{totalOriginalPrice} |
                                  VIP¥{Math.round(totalOriginalPrice * config.app.memberDiscount)} |
                                  新会员 <Text
                                  className={styles.innerCardBodyRightPriceNewMemberNumber}>¥{(totalOriginalPrice - couponData.findOne(couponDataConstants.defaultNewUserCouponData)?.flat_discount).toFixed(0)}</Text>
                                </View> : <View className={styles.innerCardBodyRightEnded}>
                                  已结束，我要催更
                                </View>
                            }
                          </View>
                        </View>
                      </Card>

                    })
                    return <>
                      {
                        view(upcomingClassGroups)
                      }
                      {
                        view(pastClassGroups)
                      }
                    </>
                  })()
                }
              </>
          }
        })()

      }
      <View className={styles.padding}/>
    </View>
    <Tab/>
    <LoadingScreen loading={isLoading}/>
  </View>
}
