import * as React from 'react'
import { useEffect, useState } from 'react'
import { Image, navigateBack, navigateTo, View } from 'remax/wechat'
import styles from './index.css'
import extendedArrowLeftIcon from '../../assets/icons/extendedarrowleft.svg'
import { verifyToken } from '../../library/user'
import LoadingScreen from '../../components/loading'
import Card from '../../components/card'
import EmptySpace from '../../components/emptySpace'
import { useShareAppMessage } from '../../library/hooks'
import { useDb } from '../../library/db'
import { dbCollections } from '../../library/constants'
import { requestGet, requestPost, requestWxPayment, requestWxShowToast } from '../../library/wxPromise'
import DisplayText from '../../components/displayTextPageTemplate'

export default () => {
  const [isLoading, setIsLoading] = useState(false)
  const [couponData, setDefaultCouponData] = useDb(dbCollections.couponDatas)
  const [, setDefaultLegalTexts] = useDb(dbCollections.legalTexts)

  useEffect(() => {
    async function _ () {
      setIsLoading(true)
      const token = await verifyToken()
      const couponData = await setDefaultCouponData(() => requestGet(`/coupon-data`))
      setIsLoading(false)
    }

    _()
  }, [])

  const onClickBuy = async () => {
    setIsLoading(true)
    const token = await verifyToken()
    const { prepay_id, paymentId, signature, requestId, timestamp } = await requestPost('/payments/allocate/weixin', {
      'classShortDescription': 'special coupon',
      'classCost': 1990
    }, {
      Authorization: `Bearer ${token}`
    })
    try {
      const { errMsg } = await requestWxPayment(prepay_id, signature, requestId, timestamp)
      console.log(`wx payment : ${errMsg}`)
      if (errMsg !== 'requestPayment:ok') {
        console.log(errMsg)
        setIsLoading(false)
        return
      }
    } catch (e) {
      console.error(e)
      setIsLoading(false)
      return
    }
    await Promise.all([
      requestPost('/user-inventories', {
        quantity: 2,
        coupon: 28
      }, {
        Authorization: `Bearer ${token}`
      }),

      requestPost('/user-inventories', {
        quantity: 6,
        coupon: 27
      }, {
        Authorization: `Bearer ${token}`
      })
    ])
    setIsLoading(false)
    await requestWxShowToast({
      title: '购买成功',
      icon: 'error',
      duration: 2000
    })
  }

  useShareAppMessage(setIsLoading)

  return (
    <View className={styles.root}>
      <View className={styles.headerBackButton} onClick={() => navigateTo({ url: '/pages/mainPage/index' })}>
        <Image src={extendedArrowLeftIcon} className={styles.headerBackButtonIcon}/>
        Back
      </View>
      <View className={styles.body}>

        <Card>
          <View className={styles.header}>
            神秘优惠
          </View>
          <View className={styles.content}>
            <View className={styles.pricesShowcase}>
              <View
                className={`${styles.pricesShowcaseElement} ${styles.pricesShowcaseElementActive}`}
              >
                <View className={styles.pricesShowcaseElementPrice}>
                  <View className={styles.pricesShowcaseElementOriginalPrice}>
                    ¥ 19.9
                  </View>
                </View>
              </View>
            </View>
          </View>
        </Card>


        <DisplayText
          noWrapper={true}
          loaderCallback={async () => {
            const legalTexts = await setDefaultLegalTexts(() => requestGet(`/legal-texts`))
            return legalTexts.findOne(({ name }) => name === '优惠说明')?.full_markdown_text
          }}/>

        <EmptySpace vertical={300}/>
        <View className={styles.footer}>
          <View className={styles.actionButtonReserve} onClick={onClickBuy}>
            立即购买
          </View>

        </View>
      </View>
      <LoadingScreen loading={isLoading}/>
    </View>
  )
};
