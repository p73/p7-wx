import * as React from 'react'
import { useEffect, useState } from 'react'
import { Image, navigateTo, redirectTo, Text, View } from 'remax/wechat'
import styles from './index.css'

import extendedArrowLeftIcon from '../../assets/icons/extendedarrowleft.svg'
import arrowDownIcon from '../../assets/icons/arrowdown.svg'

import { useQuery } from 'remax'
import { requestGet, requestPost, requestStatic, requestWxShowToast } from '../../library/wxPromise'
import { formatTimeElement } from '../../library/date'
import { verifyToken } from '../../library/user'
import LoadingScreen from '../../components/loading'
import Card from '../../components/card'
import PaymentCard from '../../components/paymentCard'
import ProfilePicture from '../../components/profilePicture'
import { useShareAppMessage } from '../../library/hooks'
import { useDb } from '../../library/db'
import { chineseDays, dbCollections } from '../../library/constants'
import { computeFinalCost, handleErrorAsJson, payWithPoints, requestSubscribedMessages } from '../../library/util'
import { config } from '../../library/config'
import CustomVideo from '../../components/customVideo'

export default () => {
  const [classDatas, setDefaultClassDatas] = useDb(dbCollections.classDatas)
  const [publicSpaces, setDefaultPublicSpaces] = useDb(dbCollections.publicSpaces)
  const [couponData, setDefaultCouponData] = useDb(dbCollections.couponDatas)
  const [userInventory, setDefaultUserInventory] = useDb(dbCollections.userInventory)

  const [expandTeacherBackground, setExpandTeacherBackground] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [selectedCoupon, setSelectedCoupon] = useState([])
  const [activeClassData, setActiveClassData] = useState()
  const [publicSpace, setPublicSpace] = useState([])
  const [classCost, setClassCost] = useState(0)

  let { id: classId, toDate } = useQuery()
  useEffect(() => {
    async function _ () {
      setIsLoading(true)
      const token = await verifyToken()
      const classDatas = await setDefaultClassDatas(() => requestGet(`/learning-classes/${classId}`), true)
      const publicSpace = await setDefaultPublicSpaces(() => requestGet(`/public-spaces`))
      await setDefaultCouponData(() => requestGet(`/coupon-data/1`))
      await Promise.all([
        setDefaultUserInventory(() => requestGet(`/user-inventories`, {
          Authorization: `Bearer ${token}`
        }), true),
      ])
      const activeClass = classDatas.findOne(classId)
      setActiveClassData(activeClass)
      setPublicSpace(publicSpaces.findOne(activeClass.public_space.id))

      setIsLoading(false)
    }

    _()
  }, [classId])

  useShareAppMessage(setIsLoading)

  useEffect(() => {
    setClassCost(computeFinalCost(userInventory, activeClassData?.price_data.original_price, selectedCoupon))
  }, [selectedCoupon, activeClassData, userInventory])

  const onClickReserve = async () => {
    // initiate login step, phone will be provided from button
    await requestSubscribedMessages()

    setIsLoading(true)
    try {
      const token = await verifyToken()
      // reserve the spot immediately
      const { id: reservationId } = await requestPost(`/reservations`, {
        'learning_class': activeClassData.id,
      }, {
        Authorization: `Bearer ${token}`
      })

      if (!(await payWithPoints({
        userInventory,
        finalCost: classCost,
        token,
        reservationIds: [reservationId],
        description: activeClassData?.name,
        couponId: selectedCoupon?.coupon?.id
      }))) return

      requestPost(`/member-reward-points-logs`, {
        pointId: config.app.pointsId.first_time_buy_class,
      }, {
        Authorization: `Bearer ${token}`
      })
      requestPost(`/member-reward-points-logs`, {
        pointId: config.app.pointsId.buy_class,
      }, {
        Authorization: `Bearer ${token}`
      })

      setIsLoading(false)
      await redirectTo({
        url: `/pages/paymentCompletePage/index?reservationId=${reservationId}`
      })
    } catch (e) {
      console.error(e)
      handleErrorAsJson(e, {
        ALREADY_PRESENT: () => {
          requestWxShowToast({
            title: '已曾购买',
            icon: 'error',
            duration: 1000
          })
        },
        NO_MORE_SPACE: () => {
          requestWxShowToast({
            title: '全部售空',
            icon: 'error',
            duration: 1000
          })
        },
        TIME_NOT_ALLOWED: () => {
          requestWxShowToast({
            title: '已停止预约',
            icon: 'error',
            duration: 1000
          })
        }
      })
    } finally {
      setIsLoading(false)
    }
  }
  return (
    <View className={styles.root}>
      <View className={styles.header}>
        {
          activeClassData?.cover_media?.mime?.includes('image') ?
            <Image src={requestStatic(activeClassData.cover_media.url)} className={styles.headerMedia}/>
            : null
        }
        {
          activeClassData?.cover_media?.mime?.includes('video') ?
            <CustomVideo src={requestStatic(activeClassData.cover_media.url)}/>
            : null
        }
        <View className={styles.headerBackButton}
              onClick={() => wx.redirectTo({ url: `/pages/placePage/index?toDate=${toDate}` })}>
          <Image src={extendedArrowLeftIcon} className={styles.headerBackButtonIcon}/>
          Back
        </View>
      </View>
      <View className={styles.body}>
        <View className={styles.titleRow}>
          {activeClassData?.name}
        </View>
        <View className={styles.tagsRow}>
          {activeClassData?.tags?.split(',').map(x => x.trim()).join('・')}
        </View>
        <Card className={styles.teacherProfileCard}>
          <View className={styles.teacherProfileCardLeft}>
            <ProfilePicture src={activeClassData?.teacher?.profile_picture?.url}/>
          </View>
          <View className={styles.teacherProfileCardRight}>
            <View className={styles.teacherProfileCardRightName}>
              {activeClassData?.teacher?.username}
            </View>
            <View
              className={`${styles.teacherProfileCardRightBackground} ${expandTeacherBackground ? styles.teacherProfileCardRightBackgroundExpandActive : ''}`}>
              {activeClassData?.teacher?.background}
            </View>
            <View
              className={styles.teacherProfileCardRightBackgroundExpand}
              onClick={() => setExpandTeacherBackground(!expandTeacherBackground)}>
              <Image src={arrowDownIcon}
                     className={`${styles.teacherProfileCardRightBackgroundExpandIcon} ${expandTeacherBackground ? styles.teacherProfileCardRightBackgroundExpandIconExpandActive : ''}`}/>
            </View>
          </View>
        </Card>
        <Card className={styles.classDetailsCard}>
          <View className={styles.classDetailsCardEntry}>
            <View className={styles.classDetailsCardLabel}>
              {/*<Image src={shirtIcon} className={styles.classDetailsCardLabelIcon}/>*/}
              课前准备
            </View>
            <Text className={styles.classDetailsCardText}>
              {activeClassData?.keqianzhunbei_description}
            </Text>
          </View>
          <View className={styles.classDetailsCardEntry}>
            <View className={styles.classDetailsCardLabel}>
              {/*<Image src={clockIcon} className={styles.classDetailsCardLabelIcon}/>*/}
              上课时间
            </View>
            {activeClassData?.starting_datetime ?
              <Text className={`${styles.classDetailsCardText} ${styles.classDetailsCardTextDate}`}>
                {(new Date(activeClassData?.starting_datetime)).getMonth() + 1}.{new Date(activeClassData?.starting_datetime).getDate()}(周{chineseDays[new Date(activeClassData?.starting_datetime).getDay()]}) {(new Date(activeClassData?.starting_datetime)).getHours()}:{formatTimeElement((new Date(activeClassData?.starting_datetime)).getMinutes())}-{(new Date(activeClassData?.ending_datetime)).getHours()}:{formatTimeElement((new Date(activeClassData?.ending_datetime)).getMinutes())}
              </Text>
              : null}
          </View>
          <View className={styles.classDetailsCardEntry}>
            <View className={styles.classDetailsCardLabel}>
              {/*<Image src={mapHollowIcon} className={styles.classDetailsCardLabelIcon}/>*/}
              上课地点
            </View>
            <Text className={styles.classDetailsCardText}>
              {publicSpace?.full_address}
            </Text>
          </View>
          <View className={styles.classDetailsCardViewMap} onClick={() => wx.openLocation({
            latitude: +publicSpace?.latitude,
            longitude: +publicSpace?.longitude,
            name: publicSpace?.name,
            address: publicSpace?.name,
          })}>
            查看地图
          </View>
        </Card>
        <PaymentCard
          originalPrice={activeClassData?.price_data.original_price}
          cost={classCost}
          setIsLoading={setIsLoading}
          selectedCoupon={selectedCoupon}
          setSelectedCoupon={setSelectedCoupon}
          caloriesBurnRate={activeClassData?.calorie_burn_rate}
          couponTypeFilter={'place'}
        />
        <Card className={styles.noticeCard}>
          <View className={`${styles.noticeCardTitle}`}>
            注意事项
          </View>
          <View className={`${styles.noticeCardText}`}>
            1. 约课后请提前15分钟到达门店，用“我的预约”中二维码扫码开门签到；
          </View>
          <View className={`${styles.noticeCardText}`}>
            2. 门店提供更衣室，不设淋浴；
          </View>
          <View className={`${styles.noticeCardText}`}>
            3. 门店内设高速无线网络。WIFI密码:p7community；
          </View>
          <View className={`${styles.noticeCardText}`}>
            4. 门店内设直饮净水，可自行携带水杯或水壶；
          </View>
          <View className={`${styles.noticeCardText}`}>
            5. 课程开始前6小时内取消预约，不支持退款；
          </View>
          <View className={`${styles.noticeCardText}`}>
            6. 课程开始前2小时，报名不满3人，系统自动通知老师和学员课程取消，费用退至钱包。
          </View>
          <View className={`${styles.noticeCardText}`}>
            7. 成功预约本课程即表示已阅读并同意 <Text className={styles.link}
                                      onClick={() => navigateTo({ url: '/pages/privacyStatementPage/index' })}>《P7
            COMMUNITY舞蹈健身公社隐私协议》</Text>及<Text className={styles.link}
                                              onClick={() => navigateTo({ url: '/pages/thingsToNotePage/index' })}>《P7
            COMMUNITY舞蹈健身公社注意事项及免责声明》</Text>。
          </View>
        </Card>
        <View className={styles.bodyPadding}/>
      </View>
      <View className={styles.footer}>
        {
          <View className={styles.actionButtonReserve} onClick={onClickReserve}>
            立即预约
          </View>
        }

      </View>
      <LoadingScreen loading={isLoading}/>
    </View>
  )
};
