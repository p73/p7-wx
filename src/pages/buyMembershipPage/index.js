import * as React from 'react'
import { useEffect, useState } from 'react'
import { Image, navigateBack, navigateTo, Text, View } from 'remax/wechat'
import styles from './index.css'
import extendedArrowLeftIcon from '../../assets/icons/extendedarrowleft.svg'
import { verifyToken } from '../../library/user'
import LoadingScreen from '../../components/loading'
import Card from '../../components/card'
import EmptySpace from '../../components/emptySpace'
import { useShareAppMessage } from '../../library/hooks'
import { useDb } from '../../library/db'
import { dbCollections } from '../../library/constants'
import spaceBenefitIcon from '../../assets/images/placeBenefit.png'
import classBenefitIcon from '../../assets/images/classBenefit.png'
import shareBenefitIcon from '../../assets/images/shareBenefit.png'
import { requestGet, requestPost, requestWxPayment, requestWxShowToast } from '../../library/wxPromise'
import { config } from '../../library/config'
import DisplayText from '../../components/displayTextPageTemplate'

export default () => {
  const [selectedPrice, setSelectedPrice] = useState()
  const [isLoading, setIsLoading] = useState(false)
  const [priceData, setDefaultPriceData] = useDb(dbCollections.priceData)
  const [couponData, setDefaultCouponData] = useDb(dbCollections.couponDatas)
  const [, setDefaultLegalTexts] = useDb(dbCollections.legalTexts)

  useEffect(() => {
    async function _ () {
      setIsLoading(true)
      const token = await verifyToken()
      const priceData = await setDefaultPriceData(() => requestGet(`/price-data?id_gte=${config.app.priceData.points[0]}&id_lte=${config.app.priceData.points[1]}`, {
        Authorization: `Bearer ${token}`
      }))
      const couponData = await setDefaultCouponData(() => requestGet(`/coupon-data`))
      setSelectedPrice(priceData.findOne(config.app.priceData.points[0]))
      setIsLoading(false)
    }

    _()
  }, [])

  const onClickBuy = async () => {
    setIsLoading(true)
    const token = await verifyToken()
    const { prepay_id, paymentId, signature, requestId, timestamp } = await requestPost('/payments/allocate/weixin', {
      'classShortDescription': selectedPrice.name,
      'classCost': selectedPrice.discounted_price * 100
    }, {
      Authorization: `Bearer ${token}`
    })
    try {
      const { errMsg } = await requestWxPayment(prepay_id, signature, requestId, timestamp)
      console.log(`wx payment : ${errMsg}`)
      if (errMsg !== 'requestPayment:ok') {
        console.log(errMsg)
        setIsLoading(false)
        return
      }
    } catch (e) {
      console.error(e)
      setIsLoading(false)
      return
    }
    if (selectedPrice?.id >= config.app.priceData.points[0] && selectedPrice?.id <= config.app.priceData.points[1]) {
      // membership points
      await requestPost('/user-inventories', {
        quantity: selectedPrice.original_price,
        coupon: config.app.memberPointCoupon
      }, {
        Authorization: `Bearer ${token}`
      })
    } else {
      // chang tiao ka
      await requestPost('/user-inventories', {
        quantity: 1,
        coupon: selectedPrice?.coupon?.id
      }, {
        Authorization: `Bearer ${token}`
      })
    }
    setIsLoading(false)
    await requestWxShowToast({
      title: '购买成功',
      icon: 'error',
      duration: 2000
    })
  }

  useShareAppMessage(setIsLoading)

  return (
    <View className={styles.root}>
      <View className={styles.headerBackButton} onClick={() => navigateTo({ url: '/pages/profilePage/index'})}>
        <Image src={extendedArrowLeftIcon} className={styles.headerBackButtonIcon}/>
        Back
      </View>
      <View className={styles.body}>
        <Card>
          <View className={styles.header}>
            P7会员VIP专享
          </View>
          <View className={styles.content}>
            <View className={styles.benefitsShowcase}>
              {
                [
                  {
                    icon: shareBenefitIcon,
                    title: '充值优惠',
                    subtitle: '多充多送'
                  },
                  {
                    icon: classBenefitIcon,
                    title: '课程89折',
                    subtitle: '年省￥1860+'
                  },
                  {
                    icon: spaceBenefitIcon,
                    title: '场地89折',
                    subtitle: '三重模式 超强音效'
                  },
                  // {
                  //   icon: shareBenefitIcon,
                  //   title: '折扣同享',
                  //   subtitle: '好友同享95折'
                  // }
                ].map(({ icon, title, subtitle }) => {
                  return <View className={styles.benefitsShowcaseElement}>
                    <Image className={styles.benefitsShowcaseElementIcon} src={icon}/>
                    <Text className={styles.benefitsShowcaseElementTitle}>{title}</Text>
                    <Text className={styles.benefitsShowcaseElementSubTitle}>{subtitle}</Text>
                  </View>
                })
              }
            </View>
          </View>
        </Card>

        <Card>
          <View className={styles.header}>
            充值开通
          </View>
          <View className={styles.content}>
            <View className={styles.pricesShowcase}>
              {
                priceData
                  .find(({ id }) => id >= config.app.priceData.points[0] & id <= config.app.priceData.points[1])
                  .map((data => {
                    const {
                      id,
                      name,
                      original_price,
                      discounted_price,
                    } = data
                    return <View
                      className={`${styles.pricesShowcaseElement} ${id === selectedPrice?.id ? styles.pricesShowcaseElementActive : ''}`}
                      onClick={() => setSelectedPrice(data)}
                    >
                      <View className={styles.pricesShowcaseElementPrice}>
                        <View className={styles.pricesShowcaseElementOriginalPrice}>
                          ¥{discounted_price}
                        </View>
                        <View
                          className={`${styles.pricesShowcaseElementDiscountedPrice} ${id === selectedPrice?.id ? styles.pricesShowcaseElementDiscountedPriceActive : ''}`}>
                          到账¥{original_price}
                        </View>
                      </View>
                    </View>
                  }))
              }
            </View>
          </View>
        </Card>


        <Card>
          <View className={styles.header}>
            购买畅跳卡(限时特价)
          </View>
          <View className={styles.content}>
            <View className={styles.pricesShowcase}>
              {
                priceData
                  .find(({ id }) => id >= config.app.priceData.cards[0] & id <= config.app.priceData.cards[1])
                  .map((data => {
                    const {
                      id,
                      name,
                      original_price,
                      discounted_price,
                    } = data
                    return <View
                      className={`${styles.pricesShowcaseElement} ${id === selectedPrice?.id ? styles.pricesShowcaseElementActive : ''}`}
                      onClick={() => setSelectedPrice(data)}
                    >
                      <View className={styles.pricesShowcaseElementPrice}>
                        <View className={styles.pricesShowcaseElementOriginalPriceLabel}>
                          {name}
                        </View>
                        <View className={styles.pricesShowcaseElementOriginalPrice}>
                          ¥{discounted_price}
                        </View>
                        <View
                          className={`${styles.pricesShowcaseElementDiscountedPrice} ${id === selectedPrice?.id ? styles.pricesShowcaseElementDiscountedPriceActive : ''}`}>
                          原价<span
                          className={`${styles.pricesShowcaseElementDiscountedPriceNumber} ${id === selectedPrice?.id ? styles.pricesShowcaseElementDiscountedPriceNumberActive : ''}`}>
                          ¥{original_price}
                          </span>
                        </View>
                      </View>
                    </View>
                  }))
              }
            </View>
          </View>
        </Card>


        <DisplayText
          noWrapper={true}
          loaderCallback={async () => {
            const legalTexts = await setDefaultLegalTexts(() => requestGet(`/legal-texts`))
            return legalTexts.findOne(({ name }) => name === '充值说明')?.full_markdown_text
          }}/>

        <EmptySpace vertical={300}/>
        <View className={styles.footer}>
          <View className={styles.actionButtonReserve} onClick={onClickBuy}>
            立即充值
          </View>

        </View>
      </View>
      <LoadingScreen loading={isLoading}/>
    </View>
  )
};
