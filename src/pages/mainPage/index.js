import * as React from 'react'
import { useState } from 'react'
import { Image, redirectTo, Text, View } from 'remax/wechat'
import styles from './index.css'

import Tab from '../../components/tab'
import Header from '../../components/header'
import EmptySpace from '../../components/emptySpace'
import { requestGet, requestPost, requestPut, requestStatic, } from '../../library/wxPromise'
import LoadingScreen from '../../components/loading'
import LocationSelector from '../../components/locationSelector'
import { getUserData, loginWithoutPhone, verifyToken, versionCheck } from '../../library/user'
import Coupon from '../../components/coupon'
import SplashScreen from '../../components/splashScreen'
import { addDays } from '../../library/date'
import { useShareAppMessage } from '../../library/hooks'
import { dbCollections } from '../../library/constants'
import { useDb } from '../../library/db'
import { placeSelectionBuilderSelectedDefault } from '../../library/places'
import { useNativeEffect, useQuery } from 'remax'
import { config } from '../../library/config'
import Checkmark from '../../assets/images/checkmarkcircle.png'
import OpenedRedPacket from '../../assets/images/openedRedPacket.svg'
import UnopenedRedPacket from '../../assets/images/unopenedRedPacket.svg'
import FailedRedPacket from '../../assets/images/failedRedPacket.svg'
import Circle from '../../assets/images/circle.png'
import { handleErrorLogging } from '../../library/error'

export default () => {
  const { code, activationCode } = useQuery()
  const [publicSpaces, setDefaultPublicSpaces] = useDb(dbCollections.publicSpaces)
  const [roomDatas, setDefaultRoomDatas] = useDb(dbCollections.roomDatas)
  const [, setDefaultLegalTexts] = useDb(dbCollections.legalTexts)
  const [formQuestions, setDefaultFormQuestions] = useDb(dbCollections.formQuestions)
  const [referrals, setDefaultReferrals] = useDb(dbCollections.referrals)
  const [userFormAnswers, setDefaultUserFormAnswers] = useDb(dbCollections.userFormAnswers)
  const [priceDatas, setDefaultPriceDatas] = useDb(dbCollections.priceData)
  const [openingHours, setDefaultOpeningHours] = useDb(dbCollections.openingHours)
  const [couponData, setDefaultCouponData] = useDb(dbCollections.couponDatas)
  const [learningClasses, setDefaultLearningClasses] = useDb(dbCollections.learningClasses)
  const [learningClassGroups, setDefaultLearningClassGroups] = useDb(dbCollections.learningClassGroups)
  const [app] = useDb(dbCollections.app)
  const [isLoading, setIsLoading] = useState(false)
  const [showSplashScreen, setShowSplashScreen] = useState(true)
  const [isFirstTimeTriggerSplashScreen, setIsFirstTimeTriggerSplashScreen] = useState(true)
  const [isNewUser, setIsNewUser] = useState(false)
  const [isNewUserSecondStep, setIsNewUserSecondStep] = useState(false)
  const [selectedCityFilter, setSelectedCityFilter] = useState(placeSelectionBuilderSelectedDefault)
  const [selectedAnswers, setSelectedAnswers] = useState({})
  const [showKaiyePopup, setShowKaiyePopup] = useState(false)
  const [showRedPacket, setShowRedPacket] = useState(0)

  const verifyActivationCode = (user, referrals, activationCode) => {
    return activationCode === 'kaiye' &&
      !referrals.findOne(({ code, referred_to_user }) => {
        return code === activationCode && referred_to_user.id === user.id
      })
  }

  const hasAnsweredForm = (userFormAnswers, user) => {
    return !userFormAnswers.findOne(({ user: answeredUser, form_question }) => {
      return user.id === answeredUser.id && config.app.formQuestions.newUserUids.includes(form_question.id)
    })
  }
  const hasOpenedRedPacket = (userFormAnswers, user) => {
      return !userFormAnswers.findOne(({ user: answeredUser, form_question }) => {
        return user.id === answeredUser.id && form_question.id === config.app.formQuestions.redPacketReadUid
      })
    }

  ;(async () => {
    // some items must be run immediately to reduce loading times
    // load in heavy duty data as soon as possible
    setDefaultPublicSpaces(() => requestGet(`/public-spaces`))
    setDefaultRoomDatas(() => requestGet(`/room-data`))
    setDefaultLegalTexts(() => requestGet(`/legal-texts`))
    setDefaultFormQuestions(() => requestGet(`/form-questions`))
    setDefaultPriceDatas(() => requestGet(`/price-data`))
    setDefaultCouponData(() => requestGet(`/coupon-data/1`))
    setDefaultOpeningHours(() => requestGet(`/opening-hours`))
    setDefaultLearningClasses(() => requestGet(`/learning-classes`))
    setDefaultLearningClassGroups(() => requestGet(`/learning-classes/groups`))
  })()

  useNativeEffect(() => {
    async function _ () {
      setIsLoading(true)
      try {
        if (app.isEmpty()) {
          setIsFirstTimeTriggerSplashScreen(true)
          await versionCheck()
          await verifyToken()
          await loginWithoutPhone()
          await app.insertFrom([{
            id: 'login',
            value: 'completed'
          }])
        } else {
          setIsFirstTimeTriggerSplashScreen(false)
        }
        const token = await verifyToken()
        const user = await getUserData(token)
        const [
          referrals,
          userFormAnswers
        ] = await Promise.all([
          setDefaultReferrals(() => requestGet(`/referrals?referred_to_user_eq=${user.id}`, {
            Authorization: `Bearer ${token}`
          }), true),
          setDefaultUserFormAnswers(() => requestGet(`/user-form-answers`, {
            Authorization: `Bearer ${token}`
          }))
        ])
        let isNewUser = wx.getStorageSync(config.storageKey.wxNewUserBegin) ? false : hasOpenedRedPacket(userFormAnswers, user)
        let isNewUserSecondStep = wx.getStorageSync(config.storageKey.wxNewUserFilledForm) ? false : hasAnsweredForm(userFormAnswers, user)
        setIsNewUser(isNewUser)
        if (!isNewUser) {
          setIsNewUserSecondStep(isNewUserSecondStep)
        }
        if (!isNewUser && !isNewUserSecondStep && verifyActivationCode(user, referrals, activationCode)) {
          setShowKaiyePopup(true)
        }
        if (code) {
          await requestPut(`/referrals`, { code }, {
            Authorization: `Bearer ${token}`
          })
        }
        setShowSplashScreen(false)
        setIsLoading(false)
      } catch (e) {
        handleErrorLogging(e, false)
      }
    }

    _()
  }, [app])

  useShareAppMessage(setIsLoading)

  const onClickNewUserSecondQuestion = async () => {
    setIsLoading(true)
    try {
      await wx.setStorageSync(config.storageKey.wxNewUserFilledForm, (new Date()).toISOString())
      const token = await verifyToken()
      await Promise.all(Object.entries(selectedAnswers).map(([questionId, answers]) => {
        return requestPost('/user-form-answers', {
          'form_question': questionId,
          'form_answers': answers,
        }, {
          Authorization: `Bearer ${token}`
        })
      }))

      requestPost(`/member-reward-points-logs`, {
        pointId: config.app.pointsId.complete_form,
      }, {
        Authorization: `Bearer ${token}`
      })

      setIsLoading(false)

      if (verifyActivationCode(await getUserData(), referrals, activationCode)) {
        setShowKaiyePopup(true)
      } else {
        await redirectTo({
          url: `/pages/mainPage/index`
        })
      }
    } catch (e) {
      handleErrorLogging(e)
    }
  }

  const onClickNewUser = async () => {
    try {

      const token = await verifyToken()
      await wx.setStorageSync(config.storageKey.wxNewUserBegin, (new Date()).toISOString())
      await requestPost('/user-form-answers', {
        form_question: config.app.formQuestions.redPacketReadUid,
        form_answers: config.app.formQuestions.redPacketReadUidAnswer,
      }, {
        Authorization: `Bearer ${token}`
      })
      const user = await getUserData(token)
      setIsNewUser(false)
      const isNewUserSecondStep = hasAnsweredForm(userFormAnswers, user)
      if (!isNewUserSecondStep) {
        setIsNewUser(false)
      } else {
        setIsNewUserSecondStep(true)
      }
    } catch (e) {
      handleErrorLogging(e)
    }
  }

  const onClickPublicSpace = async (publicSpaceId) => {
    await redirectTo({
      url: `/pages/placePage/index?publicSpaceId=${publicSpaceId}`
    })
  }

  const onClickAnswer = async (question, answer) => {
    if (selectedAnswers[question]?.find(id => answer === id)) {
      setSelectedAnswers({
        ...selectedAnswers,
        [question]: selectedAnswers[question].filter(id => answer !== id)
      })
    } else {

      setSelectedAnswers({
        ...selectedAnswers,
        [question]: [...(selectedAnswers[question] || []), answer]
      })
    }
  }

  const onClickRedPacket = async () => {
    try {

      let token = await verifyToken()
      if (verifyActivationCode(await getUserData(), referrals, activationCode)) {
        try {
          if (await requestPost(`/events/redeem`, {
            activationCode
          }, {
            Authorization: `Bearer ${token}`
          })) {
            await requestPost(`/user-inventories`, {
              quantity: 1,
              coupon: 1,
            }, {
              Authorization: `Bearer ${token}`
            })
            setShowRedPacket(1)
          }
        } catch (e) {
          console.log(e)
          setShowRedPacket(2)
        }
      }
    } catch (e) {
      handleErrorLogging(e)
    }
  }

  return (
    <View className={styles.root}>
      <Header title={'P7 COMMUNITY'}>
        <LocationSelector
          publicSpacesList={publicSpaces.find()}
          selectedCityFilter={selectedCityFilter}
          setSelectedCityFilter={setSelectedCityFilter}
        />
        <EmptySpace vertical={30}/>
      </Header>
      <View className={styles.body}>
        {
          publicSpaces?.find(({ city }) => {
            if (selectedCityFilter.id) {
              return city === selectedCityFilter.id
            }
            return true
          })?.map(({ address, name, cover_media, id: publicSpaceId, is_ready, latitude, longitude }) => {
            return <View key={name} className={styles.card}
                         onClick={is_ready ? () => onClickPublicSpace(publicSpaceId) : null}>
              {is_ready ? null : <View className={styles.cardOverlay}>
                <View className={styles.cardOverlayText}>
                  新店准备 尽请期待
                </View>
              </View>}
              <View className={styles.cardBackground} style={{
                filter: is_ready ? 'none' : 'blur(0.5vw)'
              }}>
                <Image className={styles.cardBackgroundImage} src={requestStatic(cover_media.formats.small.url)}/>
              </View>
              <View className={styles.cardFooter}>
                <View className={styles.cardFooterTop}>
                  {name} <Text className={styles.cardFooterTopDescription}>24小时营业</Text>
                </View>
                <View className={styles.cardFooterBottom}>
                  {address}
                </View>

                <View className={styles.cardFooterBottomViewMap} onClick={e => {
                  e.stopPropagation()
                  wx.openLocation({
                    latitude: +latitude,
                    longitude: +longitude,
                    name: name,
                    address: name,
                  })
                }}>
                  查看地图
                </View>
              </View>
            </View>
          })
        }
        <EmptySpace vertical={5} scaleToHeight/>
      </View>
      <Tab/>
      <LoadingScreen loading={isLoading}/>
      {
        isNewUser ?
          <View className={styles.newUserPopup}>
            <View className={styles.newUserPopupBackground}/>
            <View className={styles.newUserPopupBody}>
              <View className={styles.newUserPopupBodyHeader}>
                <Text className={styles.newUserPopupBodyHeaderTextTop}>
                  新人有礼
                </Text>
                <EmptySpace/>

                <Text>
                  <Text className={styles.newUserPopupBodyHeaderTextBottomHighlight}>
                    120元舞蹈
                  </Text>
                  代金券送给你
                </Text>
              </View>
              <View className={styles.newUserPopupBodyContent}>
                <Coupon
                  expirationDate={addDays(new Date(), 30)}
                  flatDiscount={60}
                  onClick={onClickNewUser}
                />
                <EmptySpace vertical={30}/>
                <Coupon
                  expirationDate={addDays(new Date(), 30)}
                  flatDiscount={60}
                  onClick={onClickNewUser}
                />
              </View>
              <View className={styles.newUserPopupActionButton} onClick={onClickNewUser}>
                放入卡包
              </View>
            </View>
          </View> : null
      }
      {
        isNewUserSecondStep ?
          <View className={styles.newUserPopup}>
            <View className={styles.newUserPopupBackground}/>
            <View className={styles.newUserPopupBody}>
              <View className={styles.newUserPopupBodyHeaderSecondStep}>
                <Text className={styles.newUserPopupBodyHeaderTextTop}>
                  开启你的舞蹈之旅！
                </Text>
              </View>
              <View className={styles.newUserPopupBodyContentSecondStep}>
                {formQuestions
                  .find(({ id }) => config.app.formQuestions.newUserUids.includes(id))
                  .map(({
                    id: questionId,
                    content,
                    form_answers
                  }, index) => {
                    switch (index) {
                      case 0:
                        return <View className={styles.newUserPopupBodyContentSecondStep}>
                          <View className={styles.newUserPopupBodyContentSecondStepTitle}>
                            {index + 1}. {content}
                          </View>
                          <View className={styles.newUserPopupBodyContentSecondStepAnswer}>
                            {form_answers.map(({ id: answerId, content }) => {
                              return <View
                                className={`${styles.newUserPopupBodyContentSecondStepAnswerElement} 
                            ${(selectedAnswers[questionId] || []).includes(answerId) ?
                                  styles.newUserPopupBodyContentSecondStepAnswerElementActive :
                                  ''}`}
                                onClick={() => onClickAnswer(questionId, answerId)}
                              >
                                {content}
                              </View>
                            })}
                          </View>
                        </View>
                      case 1:

                        return <View className={styles.newUserPopupBodyContentSecondStep}>
                          <View className={styles.newUserPopupBodyContentSecondStepTitle}>
                            {index + 1}. {content}
                          </View>
                          <View className={styles.newUserPopupBodyContentSecondStepAnswerSecondQuestion}>
                            {form_answers.map(({ id: answerId, content }) => {
                              const isActive = (selectedAnswers[questionId] || []).includes(answerId)
                              return <View
                                className={`${styles.newUserPopupBodyContentSecondStepAnswerElementSecondQuestion} 
                            ${isActive ?
                                  styles.newUserPopupBodyContentSecondStepAnswerElementSecondQuestionActive :
                                  ''}`}
                                onClick={() => onClickAnswer(questionId, answerId)}
                              >
                                <View>
                                  {content}
                                </View>
                                <View
                                  className={styles.newUserPopupBodyContentSecondStepAnswerElementSecondQuestionIconContainer}>
                                  <Image src={isActive ? Checkmark : Circle}
                                         className={styles.newUserPopupBodyContentSecondStepAnswerElementSecondQuestionIcon}/>

                                </View>
                              </View>
                            })}
                          </View>
                        </View>
                    }
                  })}
              </View>
              <View className={styles.newUserPopupActionButton} onClick={onClickNewUserSecondQuestion}>
                查看课表
              </View>
            </View>
          </View> : null
      }
      {
        showKaiyePopup ?
          <View className={styles.newUserPopup}>
            <View className={styles.newUserPopupBackground}/>
            <Image
              onClick={() => showRedPacket ? redirectTo({
                url: `/pages/mainPage/index`
              }) : onClickRedPacket()}
              className={styles.newUserPopupBodyContentKaiyeRedPacket}
              src={(() => {
                switch (showRedPacket) {
                  case 2:
                    return FailedRedPacket
                  case 1:
                    return OpenedRedPacket
                  case 0:
                  default:
                    return UnopenedRedPacket
                }
              })()}/>
          </View> : null
      }
      <SplashScreen show={showSplashScreen} shouldTrigger={isFirstTimeTriggerSplashScreen}/>
    </View>
  )
};
