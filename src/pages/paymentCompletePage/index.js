import * as React from 'react'
import { useEffect, useState } from 'react'
import { Image, redirectTo, Text, View } from 'remax/wechat'
import styles from './index.css'
import extendedArrowLeftIcon from '../../assets/icons/extendedarrowleft.svg'
import { useQuery } from 'remax'
import { requestGet, requestPost, requestWxShowModal, requestWxShowToast } from '../../library/wxPromise'
import { diffDaysHours, formatTimeElement } from '../../library/date'
import { verifyToken } from '../../library/user'
import LoadingScreen from '../../components/loading'
import ProfilePicture from '../../components/profilePicture'
import PlacePicture from '../../components/placePicture'
import { useShareAppMessage } from '../../library/hooks'
import { useDb } from '../../library/db'
import { dbCollections } from '../../library/constants'
import { handleErrorAsJson, requestSubscribedMessages } from '../../library/util'
import { sleep } from '../../library/promise'

export default () => {
  let { reservationId } = useQuery()
  const [isLoading, setIsLoading] = useState(false)
  const [reservation, setReservation] = useState()
  const [reservationPublicSpace, setReservationPublicSpace] = useState()
  const [, setDefaultReservation] = useDb(dbCollections.reservations)
  const [roomDatas, setDefaultRoomDatas] = useDb(dbCollections.roomDatas)
  const [publicSpaces, setDefaultPublicSpaces] = useDb(dbCollections.publicSpaces)
  const [learningClassGroups, setDefaultLearningClassGroups] = useDb(dbCollections.learningClassGroups)

  useEffect(() => {
    async function _ () {
      setIsLoading(true)
      const token = await verifyToken()
      let reservations = await setDefaultReservation(() => requestGet(`/reservations/${reservationId}`, {
        Authorization: `Bearer ${token}`
      }), true)
      await setDefaultRoomDatas(() => requestGet(`/room-data`))

      let reservation = reservations.findOne(reservationId)
      const publicSpaceId = reservation?.public_space?.id || reservation?.learning_class?.public_space
      const groupId = reservation?.learning_class?.parent_learning_class
      if (groupId) {
        await setDefaultLearningClassGroups(() => requestGet(`/learning-classes/groups/${groupId}`), true)
      }
      let publicSpaces = await setDefaultPublicSpaces(() => requestGet(`/public-spaces`))

      setReservationPublicSpace(publicSpaces.findOne(publicSpaceId))
      setReservation(reservation)
      setIsLoading(false)
    }

    _()
  }, [reservationId])

  const onCancelAction = async () => {
    await requestSubscribedMessages()
    if (await requestWxShowModal({
      title: '请确认',
      content: '取消课程，申请退款？'
    })) {
      const token = await verifyToken()
      try {
        await requestPost(`/reservations/cancel/${reservationId}`, {}, {
          Authorization: `Bearer ${token}`
        })
        await requestWxShowToast({
          title: '退课成功',
          icon: 'success',
          duration: 1000
        })
        await sleep(1500)
        await redirectTo({ url: '/pages/calendarPage/index' })
      } catch (e) {
        console.error(e)
        handleErrorAsJson(e, {
          ALREADY_PRESENT: () => {
            requestWxShowToast({
              title: '退课失败',
              icon: 'error',
              duration: 1000
            })
          },
          ALREADY_CONFIRMED: ()=>{
            requestWxShowToast({
              title: '无法退课',
              icon: 'error',
              duration: 1000
            })
          }
        })
      }
    }
  }

  useShareAppMessage(setIsLoading)

  return (
    <View className={styles.root}>
      <View className={styles.headerBackButton} onClick={() => redirectTo({ url: '/pages/calendarPage/index' })}>
        <Image src={extendedArrowLeftIcon} className={styles.headerBackButtonIcon}/>
        Back
      </View>
      <View className={styles.body}>
        <View className={styles.bodyHeader}>
          <View className={styles.bodyHeaderLeft}>
            <View className={styles.bodyHeaderLeftTitle}>
              {
                reservation?.learning_class ?
                  <View>
                    <Text>
                      {reservation.learning_class?.parent_learning_class ? learningClassGroups.findOne(reservation.learning_class?.parent_learning_class).name : reservation.learning_class.name}
                    </Text>
                    <Text
                      className={styles.bodyHeaderLeftSubTitle}>{reservation.learning_class.teacher.username}
                    </Text>
                  </View> :
                  null
              }
              {
                reservation?.public_space ?
                  '舞场预定' :
                  null
              }
            </View>
            <View className={styles.bodyHeaderLeftTime}>
              {reservation?.learning_class?.starting_datetime ?
                <Text>
                  {(new Date(reservation?.learning_class?.starting_datetime)).getMonth() + 1}月{(new Date(reservation?.learning_class?.starting_datetime)).getDate()}日 {(new Date(reservation?.learning_class?.starting_datetime)).getHours()}:{formatTimeElement((new Date(reservation?.learning_class?.starting_datetime)).getMinutes())}-{(new Date(reservation?.learning_class?.ending_datetime)).getHours()}:{formatTimeElement((new Date(reservation?.learning_class?.ending_datetime)).getMinutes())}
                </Text>
                : null}
              {
                reservation?.public_space ?
                  <Text>
                    {reservation.public_space.name}
                  </Text>
                  : null
              }
            </View>
            <View className={styles.bodyHeaderLeftAddress}>
              {reservation?.learning_class ?
                reservationPublicSpace?.address
                : null}
              {reservation?.public_space ?
                roomDatas.findOne(reservation.public_space.room).type
                : null}
            </View>
          </View>
          <View className={styles.bodyHeaderRight}>
            {
              reservation?.learning_class ? <ProfilePicture
                  src={reservation?.learning_class?.teacher?.profile_picture?.url}/>
                : null
            }
            {
              reservation?.public_space ? <PlacePicture
                  src={reservation?.public_space?.cover_media?.url}/>
                : null
            }
          </View>
        </View>
        <View className={styles.bodyContent}>
          <View
            className={styles.bodyFooterActionButton} onClick={onCancelAction}>
            取消预约
          </View>
          恭喜你预约成功{'\n'}
          开课前15min可扫码进入教室
          <Image className={styles.bodyContentQRCode} src={reservation?.login_record?.qrcode}/>
        </View>

        <View className={styles.bodyFooter}>
          <View className={styles.bodyFooterAction}>
            截图保存
          </View>
        </View>
      </View>
      <LoadingScreen loading={isLoading}/>
    </View>
  )
};
