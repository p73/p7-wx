import * as React from 'react'
import { useEffect, useState } from 'react'
import { Button, Image, navigateTo, redirectTo, Text, View } from 'remax/wechat'
import styles from './index.css'
import Tab from '../../components/tab'
import Header from '../../components/header'
import Card from '../../components/card'
import Divider from '../../components/divider'
import Scrollable from '../../components/scrollable'
import Coupon from '../../components/coupon'
import ProfilePicture from '../../components/profilePicture'
import arrowRightIcon from '../../assets/icons/arrowright.svg'
import arrowRightWhiteIcon from '../../assets/icons/arrowrightwhite.svg'
import arrowDownIcon from '../../assets/icons/arrowdown.svg'
import { getUserData, verifyToken } from '../../library/user'
import EmptySpace from '../../components/emptySpace'
import { requestGet } from '../../library/wxPromise'
import LoadingScreen from '../../components/loading'
import { useShareAppMessage } from '../../library/hooks'
import { couponDataConstants, dbCollections } from '../../library/constants'
import { useDb } from '../../library/db'
import { config } from '../../library/config'
import { getTotalMemberPoints } from '../../library/util'
import { handleErrorLogging } from '../../library/error'

export default () => {
  const [isLoading, setIsLoading] = useState(false)
  const [userInfo, setUserInfo] = useState()
  const [openCoupon, setOpenCoupon] = useState(false)
  const [openShare, setOpenShare] = useState(false)
  const [userInventory, setDefaultUserInventory] = useDb(dbCollections.userInventory)
  const [memberPointsLog, setDefaultMemberPointsLog] = useDb(dbCollections.memberPointsLog)
  const [isTeacher, setIsTeacher] = useState(false)
  useEffect(() => {
    async function _ () {
      setIsLoading(true)
      try {
        const token = await verifyToken()
        const user = await getUserData(token)
        setIsTeacher(user.user_type === 'Teacher')
        setUserInfo(JSON.parse(wx.getStorageSync(config.storageKey.wxUserProfile)))
        await Promise.all([
          setDefaultUserInventory(() => requestGet(`/user-inventories`, {
            Authorization: `Bearer ${token}`
          }), true),
          setDefaultMemberPointsLog(() => requestGet(`/member-reward-points-logs`, {
            Authorization: `Bearer ${token}`
          }), true),
        ])
      } catch (e) {
        handleErrorLogging(e)
      }
      setIsLoading(false)
    }

    _()
  }, [])

  useShareAppMessage(setIsLoading)

  const onShareClick = async (e) => {
    // refresh user inventory completely
    const token = await verifyToken()
    await setDefaultUserInventory(() => requestGet(`/user-inventories`, {
      Authorization: `Bearer ${token}`
    }), true)
  }

  return <View className={styles.root}>
    <Header
      headerClassname={styles.header}
      title={''}>
      <View className={styles.headerCustomBody}>
        <View className={styles.headerCustomBodyTop}>
          <View className={styles.headerCustomProfilePicture}>
            <ProfilePicture externalSrc={userInfo?.avatarUrl}/>
          </View>
          <View className={styles.headerCustomRight}>
            <View className={styles.headerCustomTopLine}>
              <View className={styles.headerCustomMemberText}>
                会员{userInventory.find(({ coupon }) => coupon.id === config.app.memberPointCoupon || coupon.id === config.app.memberPointUnredeemableCoupon).length ? 'VIP' : ''}卡
              </View>
            </View>
            <View className={styles.headerCustomBottomLine}>
              <View className={styles.headerCustomBottomLineName}>
                <View className={styles.headerCustomBottomLineNameUser}>{userInfo?.nickName}</View>专属
              </View>
              <View className={styles.headerCustomBottomLineAction}
                    onClick={() => navigateTo({ url: '/pages/buyMembershipPage/index' })}>
                查看特权
                <Image src={arrowRightWhiteIcon} className={styles.headerCustomBottomLineActionIcon}/>
              </View>
            </View>
            <View className={styles.headerCustomBottomLineBackground}/>
          </View>
        </View>
        <EmptySpace vertical={20}/>
        <View className={styles.headerCustomBodyBottom}>
          <View className={styles.headerCustomBodyBottomElement}>
            <View className={styles.headerCustomBodyBottomElementTop}>
              ¥{getTotalMemberPoints(userInventory)}
            </View>
            <View className={styles.headerCustomBodyBottomElementBottom}>
              卡包余额
            </View>
          </View>
          <Divider className={styles.verticalDivider}/>
          <View className={styles.headerCustomBodyBottomElement}>
            <View className={styles.headerCustomBodyBottomElementTop}>
              {
                memberPointsLog.find().map(({ member_reward_points_type: { points } }) => +points).reduce((a, b) => a + b, 0)
              }
            </View>
            <View className={styles.headerCustomBodyBottomElementBottom}>
              会员积分
            </View>

          </View>
        </View>
      </View>
      <View>

      </View>
      <EmptySpace vertical={30}/>
    </Header>
    <Scrollable height={65}>
      <Card className={styles.allOptions}>
        <View className={`${styles.allOptionsEntry} ${styles.allOptionsEntryTop}`}>
          <View className={styles.allOptionsEntryHeader}
                onClick={() => navigateTo({ url: '/pages/buyMembershipPage/index' })}>
            <View className={styles.allOptionsEntryHeaderLeft}>
              充值
            </View>
            <View className={styles.allOptionsEntryHeaderRight}>
              <View className={styles.allOptionsEntryHeaderRightLabel}>
                成为VIP，解锁更多权限
              </View>
              <Image src={arrowRightIcon} className={styles.allOptionsEntryHeaderRightIconAlternate}/>
            </View>
          </View>
        </View>
        <Divider className={styles.allOptionsEntryDivider}/>
        <View className={`${styles.allOptionsEntry}`}>
          <View className={styles.allOptionsEntryHeader} onClick={() => setOpenCoupon(!openCoupon)}>
            <View className={styles.allOptionsEntryHeaderLeft}>
              代金券
            </View>
            <View className={styles.allOptionsEntryHeaderRight}>
              <View className={styles.allOptionsEntryHeaderRightLabel}>
                {userInventory?.find(({ coupon: { id } }) => {
                  return id === couponDataConstants.defaultNewUserCouponData || id === couponDataConstants.everythingGoesCouponData || id === couponDataConstants.everythingGoesCouponData2
                })?.map(({ quantity }) => quantity)?.reduce((a, b) => a + b, 0)}张
              </View>
              <Image src={arrowDownIcon} className={styles.allOptionsEntryHeaderRightIcon}/>
            </View>
          </View>
          {
            openCoupon ?
              <View className={styles.allOptionsEntryBody}>
                {
                  userInventory?.find(({ coupon }) => coupon.id === couponDataConstants.defaultNewUserCouponData || coupon.id === couponDataConstants.everythingGoesCouponData || coupon.id === couponDataConstants.everythingGoesCouponData2)?.map(item => {
                    const { quantity } = item
                    return Array.from({ length: quantity }, () => item)
                  })?.flat()?.map(({ coupon, expiration_date }) => {
                    const { flat_discount } = coupon
                    let couponName
                    if (coupon.id === couponDataConstants.everythingGoesCouponData || coupon.id === couponDataConstants.everythingGoesCouponData2) {
                      couponName = coupon.name
                    }
                    const expirationDate = new Date(expiration_date)
                    return <Coupon
                      expirationDate={expirationDate}
                      flatDiscount={flat_discount}
                      couponName={couponName}
                      key={Math.random()}
                      onClick={() => redirectTo({ url: '/pages/placePage/index' })}
                    />
                  })
                }
              </View> : null
          }
        </View>
        <Divider className={styles.allOptionsEntryDivider}/>
        <View className={`${styles.allOptionsEntry} ${styles.allOptionsEntryBottom}`}>
          <View className={styles.allOptionsEntryHeader} onClick={() => setOpenShare(!openShare)}>
            <View className={styles.allOptionsEntryHeaderLeft}>
              邀请好友
            </View>
            <View className={styles.allOptionsEntryHeaderRight}>
              <View className={styles.allOptionsEntryHeaderRightLabelImportant}>
                立得60元代金券
              </View>
              <Image src={arrowDownIcon} className={styles.allOptionsEntryHeaderRightIcon}/>
            </View>
          </View>
          {
            openShare ?
              <View className={styles.allOptionsEntryBody}>
                <View className={styles.sharing}>
                  <Text>
                    邀请一位新好友成为会员
                  </Text>
                  <EmptySpace/>
                  <Text>
                    立刻获得60元代金券
                  </Text>
                  <View className={styles.sharingButton}>
                    立刻分享
                    <Button openType={'share'} className={styles.sharingButtonWxButton} onClick={onShareClick}/>
                  </View>
                </View>
              </View> : null
          }
        </View>
      </Card>

      {
        isTeacher ? <Card>
          <View className={`${styles.allOptionsEntry} ${styles.allOptionsEntryBottom}`}>
            <View className={styles.allOptionsEntryHeader} onClick={() => redirectTo({
              url: `/pages/teacherCalendarPage/index`
            })}>
              <View className={styles.allOptionsEntryHeaderLeft}>
                老师入口
              </View>
              <View className={styles.allOptionsEntryHeaderRight}>
                <Image src={arrowRightIcon} className={styles.allOptionsEntryHeaderRightIconAlternate}/>
              </View>
            </View>
          </View>
        </Card> : null
      }

      <Card>
        <View className={`${styles.allOptionsEntry} ${styles.allOptionsEntryBottom}`}>
          <View className={styles.allOptionsEntryHeader}>
            <View className={styles.allOptionsEntryHeaderLeft}>
              联系客服
            </View>
            <View className={styles.allOptionsEntryHeaderRight}>
              <Image src={arrowRightIcon} className={styles.allOptionsEntryHeaderRightIconAlternate}/>
            </View>
          </View>
          <Button openType={'contact'} className={styles.allOptionsEntryOverlay}/>
        </View>
      </Card>
      <EmptySpace vertical={300}/>
    </Scrollable>
    <Tab/>
    <LoadingScreen loading={isLoading}/>
  </View>
};
