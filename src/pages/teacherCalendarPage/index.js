import * as React from 'react'
import { useEffect, useState } from 'react'
import { Image, redirectTo, View } from 'remax/wechat'
import styles from './index.css'
import TeacherTab from '../../components/teacherTab'
import { requestGet } from '../../library/wxPromise'
import { formatTimeElement } from '../../library/date'
import arrowRightIcon from '../../assets/icons/arrowright.svg'
import LoadingScreen from '../../components/loading'
import Header from '../../components/header'
import EmptySpace from '../../components/emptySpace'
import ProfilePicture from '../../components/profilePicture'
import Card from '../../components/card'
import { dbCollections } from '../../library/constants'
import { useDb } from '../../library/db'
import { getUserData, verifyToken } from '../../library/user'

export default () => {
  const [roomDatas, setDefaultRoomDatas] = useDb(dbCollections.roomDatas)
  const [learningClass, setDefaultLearningClass] = useDb(dbCollections.learningClasses)
  const [displayedClasses, setDisplayedClasses] = useState([])

  const [isLoading, setIsLoading] = useState(false)
  useEffect(() => {
    async function _ () {
      setIsLoading(true)
      const learningClass = await setDefaultLearningClass(() => requestGet(`/learning-classes`))
      await setDefaultRoomDatas(() => requestGet(`/room-data`))
      const token = await verifyToken()
      const user = await getUserData(token)
      setDisplayedClasses(learningClass.find(({ teacher }) => {
        return teacher.weixin_username && teacher.weixin_username === user.weixin_username
      }))
      setIsLoading(false)
    }

    _()
  }, [])

  return (
    <View className={styles.root}>
      <Header title={'P7 COMMUNITY'}>
        <EmptySpace vertical={30}/>
      </Header>
      <View className={styles.cardBody}>
        {
          displayedClasses.sort((a, b) => {
            return new Date(b.ending_datetime || b.learning_class.ending_datetime) - new Date(a.ending_datetime || a.learning_class.ending_datetime)
          }).map(({
            id: learningClassId,
            teacher,
            starting_datetime,
            ending_datetime,
            tags,
            name,
            is_cancelled
          }) => {

            const startingDateTime = new Date(starting_datetime)
            const endingDateTime = new Date(ending_datetime)
            return <Card key={learningClassId} onClick={() => {
              if (is_cancelled) return
              return redirectTo({
                url: `/pages/teacherClassPage/index?classId=${learningClassId}`
              })
            }}>
              <View className={styles.innerCardHeader}>
                <View className={styles.innerCardHeaderLeft}>
                  <View className={styles.innerCardHeaderLeftDate}>
                    {startingDateTime.getMonth() + 1}.{startingDateTime.getDate()}
                  </View>
                  <View className={styles.innerCardHeaderLeftTime}>
                    {startingDateTime.getHours()}:{formatTimeElement(startingDateTime.getMinutes())}-{endingDateTime.getHours()}:{formatTimeElement(endingDateTime.getMinutes())}
                  </View>
                </View>
                {
                  is_cancelled ? <View className={styles.innerCardHeaderRight}>
                    已取消
                  </View> : <View className={styles.innerCardHeaderRight}>
                    查看
                    <Image src={arrowRightIcon} className={styles.innerCardHeaderRightIcon}/>
                  </View>
                }

              </View>
              <View className={styles.innerCardBody}>
                <View className={styles.innerCardBodyLeft}>
                  <ProfilePicture src={teacher?.profile_picture?.url}/>
                </View>
                <View className={styles.innerCardBodyRight}>
                  <View className={styles.innerCardBodyRightHeader}>
                    <View className={styles.innerCardBodyRightHeaderLeft}>
                      {name}
                    </View>
                    <View className={styles.innerCardBodyRightHeaderRight}>
                      {teacher?.username}
                    </View>
                  </View>
                  <View className={styles.innerCardBodyRightTags}>
                    {tags.split(',').map(x => x.trim()).join('・')}
                  </View>
                </View>
              </View>
            </Card>
          })
        }
      </View>
      <TeacherTab/>
      <LoadingScreen loading={isLoading}/>
    </View>
  )
};
