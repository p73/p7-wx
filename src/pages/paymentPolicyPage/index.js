import * as React from 'react'
import DisplayText from '../../components/displayTextPageTemplate'
import { useDb } from '../../library/db'
import { dbCollections } from '../../library/constants'
import { requestGet } from '../../library/wxPromise'

export default () => {
  const [, setDefaultLegalTexts] = useDb(dbCollections.legalTexts)

  return <DisplayText loaderCallback={async () => {
    const legalTexts = await setDefaultLegalTexts(() => requestGet(`/legal-texts`))
    return legalTexts.findOne(({ name }) => name === '充值说明')?.full_markdown_text
  }}>

  </DisplayText>
}
