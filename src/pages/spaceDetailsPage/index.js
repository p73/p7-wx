import * as React from 'react'
import { useEffect, useMemo, useState } from 'react'
import { Image, navigateTo, redirectTo, Text, View } from 'remax/wechat'
import styles from './index.css'

import extendedArrowLeftIcon from '../../assets/icons/extendedarrowleft.svg'
import crossIcon from '../../assets/icons/cross-white.svg'

import { useQuery } from 'remax'
import { requestGet, requestPost, requestStatic, requestWxShowToast } from '../../library/wxPromise'
import { verifyToken } from '../../library/user'
import LoadingScreen from '../../components/loading'
import Card from '../../components/card'
import { useShareAppMessage } from '../../library/hooks'
import { useDb } from '../../library/db'
import { chineseDays, dbCollections } from '../../library/constants'
import DaySelector from '../../components/daySelector'
import PaymentCard from '../../components/paymentCard'
import { computeFinalCost, handleErrorAsJson, payWithPoints, requestSubscribedMessages } from '../../library/util'
import CustomVideo from '../../components/customVideo'
import { config } from '../../library/config'

export default () => {
  const [publicSpaces, setDefaultPublicSpaces] = useDb(dbCollections.publicSpaces)
  const [userInventory, setDefaultUserInventory] = useDb(dbCollections.userInventory)
  const [couponData, setDefaultCouponData] = useDb(dbCollections.couponDatas)
  const [currentDay, setCurrentDay] = useState()
  const [openingHours, setDefaultOpeningHours] = useDb(dbCollections.openingHours)
  const [learningClasses, setDefaultLearningClasses] = useDb(dbCollections.learningClasses)
  const [priceDatas, setDefaultPriceDatas] = useDb(dbCollections.priceData)
  const [reservations, setDefaultReservations] = useDb(dbCollections.reservations)

  const [inactiveOpeningHours, setInactiveOpeningHours] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [selectedCoupon, setSelectedCoupon] = useState([])
  const [publicSpace, setPublicSpace] = useState(null)
  const [finalCost, setFinalCost] = useState(0)
  const [selectedDayOpeningHours, setSelectedDayOpeningHours] = useState({})
  const [selectedPrice, setSelectedPrice] = useState(0)

  let { id: publicSpaceId, date } = useQuery()
  useEffect(() => {
    setCurrentDay(new Date(date))
  }, [date])

  useEffect(() => {

    async function _ () {
      setIsLoading(true)
      const token = await verifyToken()
      let publicSpaces = await setDefaultPublicSpaces(() => requestGet(`/public-spaces`))
      await setDefaultPriceDatas(() => requestGet(`/price-data`))
      await setDefaultCouponData(() => requestGet(`/coupon-data/1`))
      await setDefaultOpeningHours(() => requestGet(`/opening-hours`))
      await setDefaultLearningClasses(() => requestGet(`/learning-classes`))
      await Promise.all([
        setDefaultUserInventory(() => requestGet(`/user-inventories`, {
          Authorization: `Bearer ${token}`
        }), true),
      ])
      setPublicSpace(publicSpaces.findOne(publicSpaceId))

      setIsLoading(false)
    }

    _()
  }, [publicSpaceId])

  useEffect(() => {
    if (!currentDay || !publicSpaceId) {
      return
    }

    async function _ () {
      const token = await verifyToken()
      let reservations = await setDefaultReservations(() => requestGet(`/reservations/all?starting_datetime_gte=${currentDay.toISOString()}`, {
        Authorization: `Bearer ${token}`
      }), true)
      let classForTheDayCount = learningClasses.find(({ starting_datetime, public_space }) => {
        if (+publicSpaceId !== +public_space?.id) {
          return false
        }
        const startingDatetime = new Date(starting_datetime)
        return startingDatetime.getDate() === currentDay.getDate() && startingDatetime.getMonth() === currentDay.getMonth() && startingDatetime.getFullYear() === currentDay.getFullYear()
      })
      let reservationsForTheDay = reservations.find(({ starting_datetime, learning_class, public_space, status }) => {
        if (status === 'Cancelled') {
          return false
        }
        // only filter them for the right space
        if (learning_class) {
          if (publicSpaceId !== learning_class.public_space) {
            return false
          }
        }

        if (public_space) {
          if (+publicSpaceId !== +public_space.id) {
            return false
          }
        }
        const startingDatetime = new Date(starting_datetime || learning_class.starting_datetime)
        return startingDatetime.getDate() === currentDay.getDate() && startingDatetime.getMonth() === currentDay.getMonth() && startingDatetime.getFullYear() === currentDay.getFullYear()
      })
      let inactiveOpeningHours = []
      classForTheDayCount.concat(reservationsForTheDay)
        .map(({ starting_datetime }) => {
          const startingDatetime = new Date(starting_datetime)
          for (let hour of openingHours.find()) {
            let { starting_time: openingHourStartingDatetime } = hour
            const [openingHour, openingMinutes] = openingHourStartingDatetime.split(':')
            if (startingDatetime.getHours() === +openingHour && startingDatetime.getMinutes() === +openingMinutes) {
              inactiveOpeningHours.push(hour)
            }
          }
        })
      setInactiveOpeningHours(inactiveOpeningHours)
    }

    _()
  }, [currentDay, publicSpaceId])

  useShareAppMessage(setIsLoading)

  useEffect(() => {
    if (!publicSpace?.room?.price_data) return
    const priceData = priceDatas.findOne(publicSpace?.room?.price_data)
    setSelectedPrice(priceData.original_price * totalOpeningHours.length)
  }, [publicSpace, selectedDayOpeningHours])

  useEffect(() => {
    if (isNaN(selectedPrice)) return
    const finalCost = computeFinalCost(userInventory, selectedPrice, selectedCoupon)
    setFinalCost(finalCost)
  }, [selectedCoupon, userInventory, selectedPrice])

  const onSelectCoupon = (inventory) => {
    setSelectedCoupon(inventory)
  }

  const onClickReserve = async () => {
    // initiate login step, phone will be provided from button
    await requestSubscribedMessages()

    if (!totalOpeningHours.length) {
      requestWxShowToast({
        title: '请先选好时间',
        icon: 'error',
        duration: 1000
      })
      return
    }

    setIsLoading(true)
    try {
      const token = await verifyToken()

      // reserve the spot immediately
      let reservationIds = await Promise.all(totalOpeningHours.map(async ([currentDay, openingHourId]) => {
        const { starting_time, ending_time } = openingHours.findOne(openingHourId)
        let startingDatetime = new Date(currentDay)
        const [startingHour, startingMinutes] = starting_time.split(':')
        startingDatetime.setHours(startingHour)
        startingDatetime.setMinutes(startingMinutes)
        startingDatetime.setSeconds(0)
        startingDatetime.setMilliseconds(0)
        let endingDatetime = new Date(currentDay)
        const [endingHour, endingMinutes] = ending_time.split(':')
        endingDatetime.setHours(endingHour)
        endingDatetime.setMinutes(endingMinutes)
        endingDatetime.setSeconds(0)
        endingDatetime.setMilliseconds(0)
        endingDatetime.setHours(endingHour)
        endingDatetime.setMinutes(endingMinutes)
        const { id: reservationId } = await requestPost(`/reservations`, {
          public_space: publicSpaceId,
          opening_hours_starting_datetime: startingDatetime,
          opening_hours_ending_datetime: endingDatetime
        }, {
          Authorization: `Bearer ${token}`
        })
        return reservationId
      }))

      if (!(await payWithPoints({
        userInventory,
        finalCost,
        token,
        reservationIds,
        description: publicSpace.name,
        couponId: selectedCoupon?.coupon?.id
      }))) return

      requestPost(`/member-reward-points-logs`, {
        pointId: config.app.pointsId.first_time_buy_place,
      }, {
        Authorization: `Bearer ${token}`
      })
      requestPost(`/member-reward-points-logs`, {
        pointId: config.app.pointsId.buy_space_per_hour,
        times: reservationIds.length
      }, {
        Authorization: `Bearer ${token}`
      })

      setIsLoading(false)
      await redirectTo({
        url: `/pages/paymentCompletePage/index?reservationId=${reservationIds[0]}`
      })
    } catch (e) {
      console.error(e)
      handleErrorAsJson(e, {
        ALREADY_PRESENT: () => {
          requestWxShowToast({
            title: '已曾购买',
            icon: 'error',
            duration: 1000
          })
        }
      })
    } finally {
      setIsLoading(false)
    }
  }

  const onClickHour = (openHoursId, day) => {
    let targetDay = currentDay
    if (day) {
      targetDay = day
    }
    if (selectedDayOpeningHours[targetDay]?.find(i => i === openHoursId)) {
      setSelectedDayOpeningHours({
        ...selectedDayOpeningHours,
        [targetDay]: selectedDayOpeningHours[targetDay].filter(i => i !== openHoursId)
      })
    } else {
      setSelectedDayOpeningHours({
        ...selectedDayOpeningHours,
        [targetDay]: [...(selectedDayOpeningHours[targetDay] || []), openHoursId]
      })
    }
  }

  const totalOpeningHours = useMemo(() => Object.entries(selectedDayOpeningHours).map(([day, hourIds]) => hourIds.map(hourId => [day, hourId])).flat(), [selectedDayOpeningHours])

  return (
    <View className={styles.root}>
      <View className={styles.header}>
        {
          publicSpace?.cover_media?.mime?.includes('image') ?
            <Image src={requestStatic(publicSpace.cover_media.url)} className={styles.headerMedia}/>
            : null
        }
        {
          publicSpace?.cover_media?.mime?.includes('video') ?
            <CustomVideo src={requestStatic(publicSpace.cover_media.url)}/>
            : null
        }
        <View className={styles.headerBackButton}
              onClick={() => wx.redirectTo({ url: `/pages/placePage/index?tab=1&publicSpaceId=${publicSpaceId}` })}>
          <Image src={extendedArrowLeftIcon} className={styles.headerBackButtonIcon}/>
          Back
        </View>
      </View>
      <View className={styles.body}>
        <View className={styles.titleRow}>
          {publicSpace?.name}
        </View>
        <View className={styles.tagsRow}>
          最多可容纳{publicSpace?.room?.max_supported_person}人
        </View>
        <DaySelector
          currentDay={currentDay}
          setCurrentDay={setCurrentDay}
        />
        <Card className={styles.openingHoursSelectorRoot}>
          <View className={styles.openingHoursSelectorHeader}>
            <View className={styles.openingHoursSelectorHeaderElement}>
              上午
            </View>
            <View className={styles.openingHoursSelectorHeaderElement}>
              下午
            </View>
            <View className={styles.openingHoursSelectorHeaderElement}>
              晚上
            </View>
          </View>
          <View className={styles.openingHoursSelector}>
            {
              openingHours.find()?.map(({ starting_time, ending_time, id: openHoursId }) => {
                let [startingTimeHours, startingTimeMinutes] = starting_time.split(':')
                let [endingTimeHours, endingTimeMinutes] = ending_time.split(':')
                let isInactive = inactiveOpeningHours.find(({ id }) => openHoursId === id)
                return <View
                  className={`${styles.openingHoursSelectorElement} 
                  ${isInactive ? styles.openingHoursSelectorElementInactive : ''} 
                  ${selectedDayOpeningHours[currentDay]?.find(id => id === openHoursId) ? styles.openingHoursSelectorElementSelected : ''}`}
                  onClick={() => onClickHour(openHoursId)}
                >
                  <View className={styles.openingHoursSelectorElementTop}>
                    {startingTimeHours}:{startingTimeMinutes}-{endingTimeHours}:{endingTimeMinutes}
                  </View>
                  <View className={styles.openingHoursSelectorElementBottom}>
                    ¥{priceDatas.findOne(publicSpace?.room?.price_data)?.original_price}
                  </View>
                </View>
              })
            }
          </View>
        </Card>
        <Card className={styles.classDetailsCard}>
          <View className={styles.classDetailsCardEntry}>
            <View className={styles.classDetailsCardLabel}>
              预约时间
            </View>
            <View className={styles.classDetailsCardContent}>
              {
                totalOpeningHours.map(([day, openingHourId]) => {
                  day = new Date(day)
                  const hour = openingHours.findOne(openingHourId)
                  const [startingHour, startingMinutes] = hour.starting_time.split(':')
                  const [endingHour, endingMinutes] = hour.ending_time.split(':')
                  return <View
                    className={styles.classDetailsCardContentEntry}
                    onClick={() => onClickHour(openingHourId, day)}

                  >
                    <View>
                      <Text>
                        {day.getMonth() + 1}.{day.getDate()}(周{chineseDays[day.getDay()]}) {startingHour}:{startingMinutes}-{endingHour}:{endingMinutes}
                      </Text>
                    </View>
                    <View className={styles.classDetailsCardContentEntryIconRoot}>
                      <Image className={styles.classDetailsCardContentEntryIcon} src={crossIcon}/>
                    </View>
                  </View>
                })
              }
            </View>
          </View>
          <View className={styles.classDetailsCardEntry}>
            <View className={styles.classDetailsCardLabel}>
              预约教室
            </View>
            <Text className={styles.classDetailsCardText}>
              {publicSpace?.full_address}
            </Text>
          </View>
          <View className={styles.classDetailsCardViewMap} onClick={() => wx.openLocation({
            latitude: +publicSpace?.latitude,
            longitude: +publicSpace?.longitude,
            name: publicSpace?.name,
            address: publicSpace?.name,
          })}>
            查看地图
          </View>
        </Card>
        <PaymentCard
          originalPrice={selectedPrice}
          cost={finalCost}
          setIsLoading={setIsLoading}
          selectedCoupon={selectedCoupon}
          setSelectedCoupon={setSelectedCoupon}
          hours={totalOpeningHours.length}
          caloriesBurnRate={400}
          couponTypeFilter={'class'}
        />
        <Card className={styles.noticeCard}>
          <View className={`${styles.noticeCardTitle}`}>
            注意事项
          </View>
          <View className={`${styles.noticeCardText}`}>
            1. 约课后请提前15分钟到达门店，用“我的预约”中二维码扫码开门签到；
          </View>
          <View className={`${styles.noticeCardText}`}>
            2. 门店提供更衣室，不设淋浴；
          </View>
          <View className={`${styles.noticeCardText}`}>
            3. 门店设沉浸式七彩动感灯光系统，如需开设，请联系客服。
          </View>
          <View className={`${styles.noticeCardText}`}>
            4. 门店音响设备设蓝牙，按第四个键连接设备蓝牙”BT-AMPLIFIER”。
          </View>
          <View className={`${styles.noticeCardText}`}>
            5. 门店提供多种音响转接口。
          </View>
          <View className={`${styles.noticeCardText}`}>
            6. 门店内设直饮纯净水，可自行携带水杯和水壶。
          </View>
          <View className={`${styles.noticeCardText}`}>
            7. 门店设自动跟拍设备，可自行根据需要使用 。
          </View>
          <View className={`${styles.noticeCardText}`}>
            8. 门店内设高速无线网络。WIFI密码:p7community；
          </View>
          <View className={`${styles.noticeCardText}`}>
            9. 用完设备和器具，请将设备和器具归回原位，保持场地干净整洁。
          </View>
          <View className={`${styles.noticeCardText}`}>
            10. 如门店操作中遇到任何问题，请与服务你的工作人员联系。
          </View>
          <View className={`${styles.noticeCardText}`}>
            11. 课程开始前6小时内取消预约，不支持退款；
          </View>
          <View className={`${styles.noticeCardText}`}>
            12. 成功预约本课程即表示已阅读并同意 <Text className={styles.link}
                                      onClick={() => navigateTo({ url: '/pages/privacyStatementPage/index' })}>《P7
            COMMUNITY舞蹈健身公社隐私协议》</Text>及<Text className={styles.link}
                                              onClick={() => navigateTo({ url: '/pages/thingsToNotePage/index' })}>《P7
            COMMUNITY舞蹈健身公社注意事项及免责声明》</Text>。
          </View>
        </Card>
        <View className={styles.bodyPadding}/>
      </View>
      <View className={styles.footer}>
        {
          <View className={styles.actionButtonReserve} onClick={onClickReserve}>
            立即预约
          </View>
        }

      </View>
      <LoadingScreen loading={isLoading}/>
    </View>
  )
};
