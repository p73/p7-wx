import * as React from 'react'
import { useEffect, useMemo, useState } from 'react'
import { Image, redirectTo, View } from 'remax/wechat'
import styles from './index.css'
import { requestGet } from '../../library/wxPromise'
import { verifyToken } from '../../library/user'
import { diffDaysHours, formatTimeElement } from '../../library/date'
import arrowRightIcon from '../../assets/icons/arrowright.svg'
import Header from '../../components/header'
import EmptySpace from '../../components/emptySpace'
import ProfilePicture from '../../components/profilePicture'
import GroupPicture from '../../components/groupPicture'
import { useShareAppMessage } from '../../library/hooks'
import Card from '../../components/card'
import { dbCollections } from '../../library/constants'
import { useDb } from '../../library/db'
import PlacePicture from '../../components/placePicture'
import { config } from '../../library/config'
import { findBoughtClassGroups } from '../../library/util'
import Tab from '../../components/tab'
import LoadingScreen from '../../components/loading'

export default () => {
  const [reservations, setDefaultReservations] = useDb(dbCollections.reservations)
  const [roomDatas, setDefaultRoomDatas] = useDb(dbCollections.roomDatas)
  const [learningClassGroups, setDefaultLearningClassGroups] = useDb(dbCollections.learningClassGroups)

  const [isLoading, setIsLoading] = useState(false)
  useEffect(() => {
    async function _ () {
      const token = await verifyToken()
      setIsLoading(true)
      await setDefaultReservations(() => requestGet(`/reservations`, {
        Authorization: `Bearer ${token}`
      }), true)
      await setDefaultLearningClassGroups(() => requestGet(`/learning-classes/groups`), true)
      await setDefaultRoomDatas(() => requestGet(`/room-data`))

      setIsLoading(false)
    }

    _()
  }, [])
  const onClickReservation = async (reservationId) => {
    await redirectTo({
      url: `/pages/paymentCompletePage/index?reservationId=${reservationId}`
    })
  }

  const learntClasses = useMemo(() => {
    if (!reservations?.find()?.length) return 0
    if (!learningClassGroups?.find()?.length) return 0

    let [, learningClassGroupTemp] = findBoughtClassGroups(reservations.find(({ user, status }) => {
      const storedUsername = wx.getStorageSync(config.storageKey.wxOpenId)
      if (status === 'Completed') {
        return user === storedUsername || user?.username === storedUsername || user?.weixin_username === storedUsername
      }
      return false
    }), learningClassGroups)

    return Object.values(learningClassGroupTemp).filter(groupClassesObj => {
      // noinspection JSCheckFunctionSignatures
      return Object.values(groupClassesObj).filter(x => !!x).length <= 0
    }).length || 0
  }, [reservations.find(), learningClassGroups.find()])

  useShareAppMessage(setIsLoading)

  return (
    <View className={styles.root}>
      <Header title={'P7 COMMUNITY'}>
        <View className={styles.headerBottomRow}>
          <View className={styles.headerBottomRowLeft}>
            <View className={styles.headerBottomRowEntry}>
              <View className={styles.headerBottomRowEntryNumber}>
                {reservations?.find(
                  ({ status }) => status === 'Completed'
                )?.length}
              </View>
              <View className={styles.headerBottomRowEntryDesc}>
                活力值
              </View>
            </View>
          </View>
          <View className={styles.headerBottomRowCenter}>
            <View className={styles.headerBottomRowEntry}>
              <View className={styles.headerBottomRowEntryNumber}>
                {reservations?.find(
                  ({ status, learning_class }) => status === 'Completed' && !!learning_class
                )?.map(
                  ({ learning_class }) => learning_class.calorie_burn_rate
                )?.reduce((a, b) => a + b, 0)}
              </View>
              <View className={styles.headerBottomRowEntryDesc}>
                已消耗卡路里
              </View>
            </View>
          </View>
          <View className={styles.headerBottomRowRight}>
            <View className={styles.headerBottomRowEntry}>
              <View className={styles.headerBottomRowEntryNumber}>
                {learntClasses}
              </View>
              <View className={styles.headerBottomRowEntryDesc}>
                累积学会曲目
              </View>
            </View>
          </View>
        </View>
        <EmptySpace vertical={30}/>
      </Header>
      <View className={styles.cardBody}>
        {
          (() => {
            if (!reservations.find()?.length) return
            let [reservationsTemp, learningClassGroupTemp] = findBoughtClassGroups(reservations.find(({
              user,
              status,
              learning_class,
              public_space,
              starting_datetime
            }) => {
              // should show future public space reservations as well
              const storedUsername = wx.getStorageSync(config.storageKey.wxOpenId)
              if (status !== 'Paid' && status !== 'Cancelled') {
                return false
              }
              if (learning_class && new Date(learning_class.starting_datetime) < new Date()) {
                return false
              }
              if (public_space && new Date(starting_datetime) < new Date()) {
                return false
              }
              return user === storedUsername || user?.username === storedUsername || user?.weixin_username === storedUsername
            }), learningClassGroups)

            return reservationsTemp.map(({
              learning_class,
              public_space,
              reservationId,
              starting_datetime,
              ending_datetime,
              status
            }) => {
              if (learning_class) {
                const {
                  name,
                  tags,
                  teacher,
                  starting_datetime,
                  ending_datetime,
                  calorie_burn_rate,
                  parent_learning_class,
                } = learning_class

                let classGroup
                if (parent_learning_class) {
                  if (!Object.keys({ ...learningClassGroupTemp[parent_learning_class] }).length) {
                    classGroup = learningClassGroups.findOne(parent_learning_class)
                  }
                }

                const startingDateTime = new Date(starting_datetime)
                const endingDateTime = new Date(ending_datetime)
                return <Card className={styles.innerCard} key={reservationId}
                             onClick={status === 'Cancelled' ? () => {} : () => onClickReservation(reservationId)}>
                  <View className={styles.innerCardHeader}>
                    <View className={styles.innerCardHeaderLeft}>
                      <View className={styles.innerCardHeaderLeftDate}>
                        {startingDateTime.getMonth() + 1}.{startingDateTime.getDate()}
                      </View>
                      <View className={styles.innerCardHeaderLeftTime}>
                        {startingDateTime.getHours()}:{formatTimeElement(startingDateTime.getMinutes())}-{endingDateTime.getHours()}:{formatTimeElement(endingDateTime.getMinutes())}
                      </View>
                    </View>
                    {
                      status === 'Cancelled' ?
                        <View className={styles.innerCardHeaderRight}>
                          已取消
                          <View className={styles.innerCardHeaderRightIcon}/>
                        </View> :
                        <View className={styles.innerCardHeaderRight}>
                          查看预约
                          <Image src={arrowRightIcon} className={styles.innerCardHeaderRightIcon}/>
                        </View>
                    }
                  </View>
                  <View className={styles.innerCardBody}>
                    <View className={styles.innerCardBodyLeft}>
                      {
                        classGroup ?
                          <GroupPicture src={classGroup?.thumbnail_media?.url || teacher?.profile_picture?.url}/>
                          : <ProfilePicture src={teacher?.profile_picture?.url}/>
                      }

                    </View>
                    <View className={styles.innerCardBodyRight}>
                      <View className={styles.innerCardBodyRightHeader}>
                        <View className={styles.innerCardBodyRightHeaderLeft}>
                          {classGroup?.name || name}
                        </View>
                        <View className={styles.innerCardBodyRightHeaderRight}>
                          {teacher?.username}
                        </View>
                      </View>
                      <View className={styles.innerCardBodyRightTags}>
                        {tags?.split(',')?.map(x => x.trim())?.join('・')}
                      </View>
                      <View className={styles.innerCardBodyRightCalorieBurnRate}>
                        可消耗{(diffDaysHours(startingDateTime, endingDateTime) * calorie_burn_rate).toFixed(0)}卡路里
                      </View>
                    </View>
                  </View>
                </Card>
              }
              if (public_space) {
                const startingDateTime = new Date(starting_datetime)
                const endingDateTime = new Date(ending_datetime)
                const {
                  cover_media,
                  room,
                  name
                } = public_space
                return <Card className={styles.innerCard} key={reservationId}
                             onClick={() => onClickReservation(reservationId)}>
                  <View className={styles.innerCardHeader}>
                    <View className={styles.innerCardHeaderLeft}>
                      <View className={styles.innerCardHeaderLeftDate}>
                        {startingDateTime.getMonth() + 1}.{startingDateTime.getDate()}
                      </View>
                      <View className={styles.innerCardHeaderLeftTime}>
                        {startingDateTime.getHours()}:{formatTimeElement(startingDateTime.getMinutes())}-{endingDateTime.getHours()}:{formatTimeElement(endingDateTime.getMinutes())}
                      </View>
                    </View>
                    <View className={styles.innerCardHeaderRight}>
                      查看预约
                      <Image src={arrowRightIcon} className={styles.innerCardHeaderRightIcon}/>
                    </View>
                  </View>
                  <View className={styles.innerCardBody}>
                    <View className={styles.innerCardBodyLeft}>
                      <PlacePicture src={cover_media?.formats.small.url}/>
                    </View>
                    <View className={styles.innerCardBodyRight}>
                      <View className={styles.innerCardBodyRightHeader}>
                        <View className={styles.innerCardBodyRightHeaderLeft}>
                          {name}
                        </View>
                      </View>
                      <View className={styles.innerCardBodyRightTags}>
                        {roomDatas.findOne(room).type}
                      </View>
                      <View className={styles.innerCardBodyRightCalorieBurnRate}>
                        可消耗{(diffDaysHours(startingDateTime, endingDateTime) * 400).toFixed(0)}卡路里
                      </View>
                    </View>
                  </View>
                </Card>
              }
            })
          })()
        }
        {
          reservations.find(({ user, status, learning_class }) => {
            if (status === 'Pending') return false
            const storedUsername = wx.getStorageSync(config.storageKey.wxOpenId)
            if (!learning_class) {
              return false
            }
            if (new Date(learning_class.starting_datetime) > new Date()) {
              return false

            }
            return user === storedUsername || user?.username === storedUsername || user?.weixin_username === storedUsername

          }).length ? <View className={styles.historyText}>历史记录</View> : null
        }
        {
          reservations.find(({ user, status, learning_class }) => {
            if (status === 'Pending') return false
            const storedUsername = wx.getStorageSync(config.storageKey.wxOpenId)
            if (!learning_class) {
              return false
            }
            if (new Date(learning_class.starting_datetime) > new Date()) {
              return false

            }
            return user === storedUsername || user?.username === storedUsername || user?.weixin_username === storedUsername

          })?.sort((a, b) => {
            return new Date(b.ending_datetime || b.learning_class.ending_datetime) - new Date(a.ending_datetime || a.learning_class.ending_datetime)
          })?.filter(r => {
            return Object.keys(r.learning_class).length
          })?.map(r => {
            return {
              reservationId: r.id,
              ...r,
            }
          })?.map(({ learning_class, reservationId, starting_datetime, ending_datetime, status }) => {
            const {
              name,
              tags,
              teacher,
              calorie_burn_rate
            } = learning_class
            const startingDateTime = new Date(starting_datetime)
            const endingDateTime = new Date(ending_datetime)
            return <Card className={styles.innerCard} key={reservationId}
                         onClick={status === 'Completed' ? () => onClickReservation(reservationId) : () => {}}>
              <View className={styles.innerCardHeader}>
                <View className={styles.innerCardHeaderLeft}>
                  <View className={styles.innerCardHeaderLeftDate}>
                    {startingDateTime.getMonth() + 1}.{startingDateTime.getDate()}
                  </View>
                  <View className={styles.innerCardHeaderLeftTime}>
                    {startingDateTime.getHours()}:{formatTimeElement(startingDateTime.getMinutes())}-{endingDateTime.getHours()}:{formatTimeElement(endingDateTime.getMinutes())}
                  </View>
                </View>
                <View className={styles.innerCardHeaderRight}>
                  {
                    status === 'Completed' ? '已完成' : '已过期'
                  }
                  {
                    status === 'Completed' ? <Image src={arrowRightIcon}
                                                    className={styles.innerCardHeaderRightIcon}/> : null
                  }
                </View>
              </View>
              <View className={styles.innerCardBody}>
                <View className={styles.innerCardBodyLeft}>
                  <ProfilePicture src={teacher?.profile_picture?.url}/>
                </View>
                <View className={styles.innerCardBodyRight}>
                  <View className={styles.innerCardBodyRightHeader}>
                    <View className={styles.innerCardBodyRightHeaderLeft}>
                      {name}
                    </View>
                    <View className={styles.innerCardBodyRightHeaderRight}>
                      {teacher?.username}
                    </View>
                  </View>
                  <View className={styles.innerCardBodyRightTags}>
                    {tags?.split(',').map(x => x.trim()).join('・')}
                  </View>
                  <View className={styles.innerCardBodyRightCalorieBurnRate}>
                    可消耗{(diffDaysHours(startingDateTime, endingDateTime) * calorie_burn_rate).toFixed(0)}卡路里
                  </View>
                </View>
              </View>
            </Card>
          })
        }
        <EmptySpace vertical={5} scaleToHeight={true}/>
      </View>
      <Tab/>
      <LoadingScreen loading={isLoading}/>
    </View>
  )
};
