import { usePageEvent } from 'remax/macro'
import { verifyToken } from './user'
import { requestPost } from './wxPromise'
import newUserImage from '../assets/images/newUser.jpg'
import { config } from './config'
import { handleErrorLogging } from './error'

export const useShareAppMessage = (setIsLoading, loggedInCallback = () => {}) => usePageEvent('onShareAppMessage', async () => {
  setIsLoading(true)
  // here the user might not have logged in.
  try {
    const userLoginToken = await verifyToken()
    wx.setStorageSync(config.storageKey.wxNewUserShared, 'true')
    const { code } = await requestPost(`/referrals`, {}, {
      Authorization: `Bearer ${userLoginToken}`
    })
    loggedInCallback()
    setIsLoading(false)
    return {
      title: '送你P7 COMMUNITY舞蹈健身公社120元新人券，快来体验！',
      path: `/pages/mainPage/index?code=${code}`,
      imageUrl: newUserImage
    }
  } catch (e) {
    handleErrorLogging(e)
  }
})
