export const config = {
  mode: 'PROD',
  version: '1.9.10',
  app: {
    memberDiscount: 0.89,
    memberPlaceDiscount: 0.89,
    memberPointCoupon: 21,
    memberPointUnredeemableCoupon: 22,
    placeDiscount: 0.95,
    priceData: {
      points: [15, 18],
      cards: [19, 22]
    },
    giftMemberPointUnredeemableCoupon: 3,
    formQuestions: {
      newUserUids: [1, 2],
      redPacketReadUid: 3,
      redPacketReadUidAnswer: 24,
      classGroupSubscriptionId: 4,
      teacherQuestions: [5, 6, 7]
    },
    blackListedCouponsForExtraPoints: [11, 20],
    templateIds: [
      // 'JXIVQcGfBfdjgrV-rMj0MSFpAf-TGFK2vc653AHxy8U',
      'X8x_l-DOp8MvCB_5Z8GeXsn7HKjG2fhnCiYBdTOt7ow'
    ],
    pointsId: {
      first_time_buy_class: 1,
      first_time_buy_class_group: 2,
      first_time_buy_place: 3,
      buy_class: 4,
      buy_space_per_hour: 5,
      complete_form: 6,
    }
  },
  storageKey: {
    wxNewUserShared: 'wxNewUserShared',
    wxNewUserBegin: 'wxNewUserBegin',
    wxUserProfile: 'wxUserProfile',
    wxUserData: 'wxUserData',
    wxSessionKey: 'wxSessionKey',
    wxOpenIdCode: 'wxOpenIdCode',
    wxOpenId: 'wxOpenId',
    version: 'version',
    mockPhone: 'mock-phone',
    token: 'token',
    tokenExpiration: 'token-expiration',
    collectedPhone: 'collected-phone',
    wxNewUserReceivedSignUpBonus: 'wxNewUserReceivedSignUpBonus',
    wxActivatedCodes: 'wxActivatedCodes',
    wxNewUserFilledForm: 'wxNewUserFilledForm'
  }
}
