import { config } from './config'
import {
  requestDelete,
  requestPost,
  requestPut,
  requestWxPayment,
  requestWxShowToast,
  requestWxSubscribeMessage
} from './wxPromise'
import { handleErrorLogging } from './error'

export const findBoughtClassGroups = (reservations, learningClassGroups) => {

  const reservationsTemp = reservations?.sort((a, b) => {
    return new Date(b.starting_datetime || b.learning_class.starting_datetime) - new Date(a.starting_datetime || a.learning_class.starting_datetime)
  })?.map(r => {
    return {
      reservationId: r.id,
      ...r,
    }
  })?.filter(a => {
    return a.public_space || Object.keys(a.learning_class).length
  })

  // check for learning class groups that are connected with the reservations
  let learningClassGroupTemp = {}
  for (const { id, learning_classes } of learningClassGroups.find()) {
    learningClassGroupTemp[id] = learning_classes.map(({ id }) => id).reduce((acc, cur) => {
      acc[cur] = true
      return acc
    }, {})
  }

  if (!Object.keys(learningClassGroupTemp).length) return [
    reservationsTemp,
    learningClassGroupTemp
  ]
  for (const { learning_class } of reservationsTemp) {
    let parent = learning_class?.parent_learning_class
    if (!parent) continue
    if (!learningClassGroupTemp[parent]) continue
    if (learningClassGroupTemp[parent][learning_class.id]) {
      learningClassGroupTemp[parent][learning_class.id] = null
    }
  }
  return [
    reservationsTemp,
    learningClassGroupTemp
  ]
}

export const getTotalMemberPoints = (userInventory) => userInventory
  .find(({ coupon }) => coupon.id === config.app.memberPointCoupon || coupon.id === config.app.memberPointUnredeemableCoupon)
  .map(({ quantity }) => quantity)
  .reduce((a, b) => a + b, 0) || 0

export const payWithPoints = async ({
  userInventory,
  finalCost,
  token,
  reservationIds,
  description,
  couponId
}) => {
  try {
    const memberPoints = getTotalMemberPoints(userInventory)
    const actualCost = memberPoints > finalCost ? 0 : finalCost - memberPoints
    let rawData = {}
    if (actualCost) {
      // first off we fill in the wallet with the right amount of coupons

      // prepare the allocation
      const {
        prepay_id,
        paymentId,
        signature,
        requestId,
        timestamp
      } = await requestPost('/payments/allocate/weixin', {
        'classShortDescription': description,
        'classCost': actualCost * 100
      }, {
        Authorization: `Bearer ${token}`
      })

      rawData = {
        prepay_id,
        paymentId,
        signature,
        requestId,
        timestamp
      }

      // then request for wx payment
      try {
        const { errMsg } = await requestWxPayment(prepay_id, signature, requestId, timestamp)
        console.log(`wx payment : ${errMsg}`)
        if (errMsg !== 'requestPayment:ok') {
          await Promise.all(reservationIds.map(async reservationId => {
            await requestDelete(`/reservations/${reservationId}`, {
              Authorization: `Bearer ${token}`
            })
          }))
          requestWxShowToast({
            title: '支付取消',
            icon: 'error',
            duration: 1000
          })
          return false
        }
      } catch (e) {
        console.error(e)
        await Promise.all(reservationIds.map(async reservationId => {
          await requestDelete(`/reservations/${reservationId}`, {
            Authorization: `Bearer ${token}`
          })
        }))
        requestWxShowToast({
          title: '支付取消',
          icon: 'error',
          duration: 1000
        })
        return false
      }

      // fill the wallet
      await requestPost('/user-inventories', {
        quantity: actualCost,
        coupon: config.app.memberPointCoupon
      }, {
        Authorization: `Bearer ${token}`
      })
    }

    // now then use the wallet to pay for the item
    const { id: paymentId } = await requestPost(`/payments`, {
      user: wx.getStorageSync(config.storageKey.wxOpenId),
      rawData: {
        ...rawData,
        amount: {
          total: finalCost * 100,
          currency: 'CNY'
        },
        payer: {
          openid: wx.getStorageSync(config.storageKey.wxOpenId)
        }
      }
    }, {
      Authorization: `Bearer ${token}`
    })

    // if the final cost is 0, we don't need to use anything
    if (finalCost > 0) {
      // use up all unredeemables
      let unredeemableQuantity = userInventory.findOne(config.app.memberPointUnredeemableCoupon)?.quantity || 0
      if (unredeemableQuantity > finalCost) {
        await requestPost(`/user-inventories/coupon/use`, {
          coupon: config.app.memberPointUnredeemableCoupon,
          quantity: finalCost
        }, {
          Authorization: `Bearer ${token}`
        })
      } else {
        if (unredeemableQuantity) {
          await requestPost(`/user-inventories/coupon/use`, {
            coupon: config.app.memberPointUnredeemableCoupon,
            quantity: unredeemableQuantity
          }, {
            Authorization: `Bearer ${token}`
          })
        }
        await requestPost(`/user-inventories/coupon/use`, {
          coupon: config.app.memberPointCoupon,
          quantity: finalCost - unredeemableQuantity
        }, {
          Authorization: `Bearer ${token}`
        })
      }
    }

    if (couponId) {
      await requestPost(`/user-inventories/coupon/use`, {
        coupon: couponId
      }, {
        Authorization: `Bearer ${token}`
      })
      if (!config.app.blackListedCouponsForExtraPoints.includes(couponId)) {
        // provide unredeemable points for each payment
        // unless it's of a specific type
        await requestPost('/user-inventories', {
          quantity: config.app.giftMemberPointUnredeemableCoupon,
          coupon: config.app.memberPointUnredeemableCoupon
        }, {
          Authorization: `Bearer ${token}`
        })
      }
    }
    // update reservation with payment id
    await Promise.all(reservationIds.map(async reservationId => {
      await requestPut(`/reservations/${reservationId}`, {
        payment: paymentId
      }, {
        Authorization: `Bearer ${token}`
      })
    }))
    // get qr code
    await Promise.all(reservationIds.map(async reservationId => {
      await requestPost(`/login-records`, {
        reservation: reservationId
      }, {
        Authorization: `Bearer ${token}`
      })
    }))
    return true
  } catch (e) {
    handleErrorLogging(e)
  }
}

export const handleErrorAsJson = (error, callbackCollection) => {
  try {
    if (error.message) {
      return callbackCollection[JSON.parse(error.message).message]()
    }
  } catch (e) {
    console.error(e)
  }
}

export const requestSubscribedMessages = async () => {
  // const target = config.app.templateIds.filter(templateId => {
  //   const requestedBefore = requestedPermissions.findOne(({ type }) => {
  //     return type === templateId
  //   })
  //   return !requestedBefore
  // })
  //
  // if (!target.length) {
  //   return
  // }

  await requestWxSubscribeMessage(config.app.templateIds)
  // await Promise.all(target.map(async id => {
  //   await requestPost(`/requested-permissions`, {
  //     type: id
  //   }, {
  //     Authorization: `Bearer ${await verifyToken()}`
  //   })
  // }))
}

export const computeFinalCost = (userInventory, originalPrice, selectedCoupon) => {
  originalPrice = +originalPrice
  if (originalPrice <= 0) return originalPrice
  if (isNaN(originalPrice)) return originalPrice
  const isMember = userInventory.findOne(({ coupon }) => {
    return coupon.id === config.app.memberPointCoupon
  })?.quantity
  let cost = originalPrice * (isMember ? config.app.memberDiscount : 1)
  if (selectedCoupon) {
    cost = (cost * (1 - selectedCoupon?.coupon?.percent_discount / 100) - selectedCoupon?.coupon?.flat_discount || 0)
  }
  if (isNaN(cost)) {
    return originalPrice
  }
  return Math.round(cost)
}

export const hasNotch = () => wx.getSystemInfoSync().statusBarHeight > 20
