import { publish, subscribe, unsubscribe } from './globalMessageQueue'
import { useEffect, useState } from 'react'

const db = {
  createCollection: function (key) {
    if (this[key]) {
      return this[key]
    }
    this[key] = {
      values: {},
      loadingLock: false,
      insertFrom: function (itemList, refresh) {
        const isSingleItem = !Array.isArray(itemList)
        if (isSingleItem) {
          itemList = [itemList]
        }
        if (refresh && !isSingleItem) {
          this.values = {}
        }
        for (const item of itemList) {
          this.values[item.id] = item
        }
        publish(`db-${key}`, itemList)
      },
      findOne: function (itemId) {
        if (!itemId && Object.values(this.values).length) {
          return Object.values(this.values)[0]
        }
        if (isNaN(+itemId)) {
          const filters = itemId
          return Object.values(this.values).filter(filters).sort((a, b) => {
            return a.id < b.id ? -1 : a.id > b.id ? 1 : 0
          })[0]
        }
        return this.values[itemId]
      },
      find: function (filters) {
        if (!filters) {
          return Object.values(this.values)
        }
        return Object.values(this.values).filter(filters).sort((a, b) => {
          return a.id < b.id ? -1 : a.id > b.id ? 1 : 0
        })
      },
      isEmpty: function () {
        return !Object.keys(this.values).length
      },
      listen: function (callback) {
        subscribe(`db-${key}`, callback)
        return () => unsubscribe(`db-${key}`, callback)
      }
    }
    return this[key]
  }
}

export const collectionFrom = (key) => {
  return db.createCollection(key)
}

export const useDb = (key) => {
  const [, setV] = useState(null)
  const collection = db.createCollection(key)
  useEffect(() => {
    return collection.listen(value => {
      setV(value)
    })
  }, [])
  return [collection, async (setterCallback, refresh) => {
    if (refresh || collection.isEmpty() && !collection.loadingLock) {
      collection.loadingLock = true
      collection.insertFrom(await setterCallback())
      collection.loadingLock = false
    }
    return collection
  }]
}
