export const placeSelectionBuilderSelectedDefault = {
  id: null,
  name: '全国'
}
export const placeSelectionBuilder = (publicSpaces) => {
  return [...new Set([null, ...publicSpaces.map(({ city }) => city)])].map(city => {
    if (!city) {
      return placeSelectionBuilderSelectedDefault
    }
    return {
      id: city,
      name: city
    }
  })
}
