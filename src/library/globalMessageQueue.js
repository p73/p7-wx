const events = {}

export const publish = function (name, data) {
  if (!!events[name] === false) return
  events[name].forEach(function (handler) {
    handler.call(this, data)
  })
}

export const subscribe = function (name, handler) {
  if (!!events[name] === false) {
    events[name] = []
  }
  events[name] = [...events[name], handler]

}

export const unsubscribe = function (name, handler) {
  const handlerIdx = events[name].indexOf(handler)
  events[name].splice(handlerIdx)
}

export const messages = {

}
