// manually generated
export const dbCollections = {
  publicSpaces: 'public-spaces',
  userInventory: 'user-inventory',
  learningClasses: 'learning-classes',
  roomDatas: 'room-datas',
  couponDatas: 'coupon-datas',
  openingHours: 'opening-hours',
  classDatas: 'class-datas',
  reservations: 'reservations',
  priceData: 'price-data',
  legalTexts: 'legal-text',
  formQuestions: 'form-questions',
  learningClassGroups: 'learning-class-groups',
  app: 'app',
  referrals: 'referrals',
  userFormAnswers: 'user-form-answers',
  requestedPermissions: 'requested-permissions',
  loginRecord: 'login-record',
  memberPointsLog: 'member-points-log',
  teacherSelectedOpeningHours: 'teacher-selected-opening-hours',
}

export const couponDataConstants = {
  defaultNewUserCouponData: 1,
  everythingGoesCouponData: 11,
  everythingGoesCouponData2: 20
}

export const chineseDays = [
  '日',
  '一',
  '二',
  '三',
  '四',
  '五',
  '六'
]
