export function addDays (date, days) {
  let result = new Date(date)
  result.setDate(result.getDate() + days)
  return result
}

export function getUnixTime () {
  return parseInt((new Date().getTime() / 1000).toFixed(0))
}

export function formatTimeElement (min) {
  min = '' + min
  if (min.length < 2) {
    return '0' + min
  } else {
    return min
  }
}

export function diffDaysHours (date1, date2) {
  const diffTime = Math.abs(date2 - date1)
  return Math.ceil(diffTime / (1000 * 60 * 60))
}

export function isSameDateWithoutTime (date1, date2) {
  return date1.getMonth() === date2.getMonth() &&
    date1.getDate() === date2.getDate() &&
    date1.getFullYear() === date2.getFullYear()
}
