import { config } from './config'
import { sleep } from './promise'
import { bindUserProfile } from './user'


// export const host = 'http://localhost:1337'
// export const host = 'http://13.67.107.40:1337'

export const host = 'https://www.p7dance.com/_api'
// export const host = 'https://p7community.southeastasia.cloudapp.azure.com'

export const cache = {}

export function requestGet (url, header = {}) {
  return new Promise((async (resolve, reject) => {
    wx.request({
      url: `${host}${url}`,
      method: 'GET',
      success: ({ data }) => resolve(data),
      fail: reject,
      header: header
    })
  }))
}

export async function requestPost (url, data, header = {}) {
  const resp = await new Promise((async (resolve, reject) => {
    wx.request({
      url: `${host}${url}`,
      method: 'POST',
      data: data,
      success: ({ data }) => resolve(data),
      fail: reject,
      header: header
    })
  }))
  if (resp.error) {
    throw new Error(JSON.stringify(resp))
  }
  return resp
}

export function requestPut (url, data, header = {}) {
  return new Promise((async (resolve, reject) => {
    wx.request({
      url: `${host}${url}`,
      method: 'PUT',
      data: data,
      success: ({ data }) => resolve(data),
      fail: reject,
      header: header
    })
  }))
}

export function requestDelete (url, header = {}) {
  return new Promise((async (resolve, reject) => {
    wx.request({
      url: `${host}${url}`,
      method: 'DELETE',
      success: ({ data }) => resolve(data),
      fail: reject,
      header: header
    })
  }))
}

export function requestStatic (url) {
  return `${host}${url}`
}

export function requestWxRefreshCredentials () {
  wx.removeStorageSync(config.storageKey.wxOpenIdCode)
  wx.removeStorageSync(config.storageKey.wxSessionKey)
}

export function requestWxLogin () {
  return new Promise(((resolve, reject) => {
    wx.checkSession({
      success (res) {
        if (!wx.getStorageSync(config.storageKey.wxOpenIdCode)) {
          wx.login({
            async success (res) {
              wx.setStorageSync(config.storageKey.wxOpenIdCode, res.code)
              const { session_key, openid } = await requestPost(`/payments/verify/weixin`, {
                wxOpenIdCode: wx.getStorageSync(config.storageKey.wxOpenIdCode)
              })
              wx.setStorageSync(config.storageKey.wxSessionKey, session_key)
              wx.setStorageSync(config.storageKey.wxOpenId, openid)
              resolve(res.code)
            },
            fail (e) {
              console.error(e)
              wx.removeStorageSync(config.storageKey.wxOpenIdCode)
              reject()
            }
          })
        } else {
          resolve(wx.getStorageSync(config.storageKey.wxOpenIdCode))
        }
      },
      fail () {
        requestWxRefreshCredentials()
        wx.login({
          async success (res) {
            wx.setStorageSync(config.storageKey.wxOpenIdCode, res.code)
            const { session_key, openid } = await requestPost(`/payments/verify/weixin`, {
              wxOpenIdCode: wx.getStorageSync(config.storageKey.wxOpenIdCode)
            })
            wx.setStorageSync(config.storageKey.wxSessionKey, session_key)
            wx.setStorageSync(config.storageKey.wxOpenId, openid)
            resolve(res.code)
          },
          fail (e) {
            console.error(e)
            wx.removeStorageSync(config.storageKey.wxOpenIdCode)
            reject()
          }
        })
      }
    })
  }))
}

export function requestWxPayment (prepay_id, signature, requestId, timestamp) {
  return new Promise((async (resolve, reject) => {
    wx.requestPayment({
      timeStamp: '' + timestamp,
      nonceStr: '' + requestId,
      package: `prepay_id=${prepay_id}`,
      signType: 'RSA',
      paySign: '' + signature,
      success: resolve,
      fail: reject
    })
  }))
}

export function requestWxUserProfile () {
  const userProfile = wx.getStorageSync(config.storageKey.wxUserProfile)
  if (userProfile) {
    return userProfile
  }
  return new Promise(((resolve, reject) => {
    return wx.getUserProfile({
      desc: '欢饮回来',
      success: async res => {
        const data = await requestPost('/profile/decrypt/weixin', {
          sessionKey: wx.getStorageSync(config.storageKey.wxSessionKey),
          encryptedData: res.encryptedData,
          iv: res.iv
        })
        wx.setStorageSync(config.storageKey.wxUserProfile, JSON.stringify(data))
        await bindUserProfile()
        resolve(data)
      },
      fail: reject
    })
  }))
}

export function requestWxCopy (data) {
  return new Promise(((resolve, reject) => {
    wx.setClipboardData({
      data: data,
      async success (res) {
        await Promise.all([
          wx.vibrateShort({ type: 'medium' }),
          wx.showToast({
            title: '连接复制成功',
            icon: 'success',
            duration: 1000
          })
        ])
        resolve(res)
      },
      fail: reject
    })
  }))
}

export async function requestWxShowToast (opts) {
  wx.showToast(opts)
  await sleep(opts.duration - 500)
}

export function requestWxShowShareMenu (opts) {
  wx.showShareMenu({
    withShareTicket: true,
  })
}

export async function requestWxSubscribeMessage (templateIds) {
  return new Promise((resolve, reject) => {
    wx.requestSubscribeMessage({
      tmplIds: templateIds,
      async success (res) {
        resolve(res)
      }, fail (err) {
        reject(err)
      }
    })
  })
}

export async function requestWxShowModal ({ title, content }) {
  return new Promise((resolve, reject) => {
    wx.showModal({
      title, content,
      success (res) {
        if (res.confirm) {
          resolve(true)
        } else if (res.cancel) {
          resolve(false)
        }
      },
      fail (e) {
        reject(e)
      }
    })
  })
}
