import { getUserData, verifyToken } from './user'
import { requestPost, requestWxShowToast } from './wxPromise'

export const handleErrorLogging = async (err, showToast = true) => {
  let user
  let getUserError
  try {
    user = await getUserData(await verifyToken())
  } catch (e) {
    getUserError = e
  }
  if(showToast) {
    requestWxShowToast({
      title: '发现问题',
      icon: 'error',
      duration: 1000
    })
  }
  await requestPost(`/error-logs`, {
    user: {
      id: user?.id,
      username: user?.username,
      weixin_username: user?.weixin_username,
    },
    message: {
      err: {
        message: err?.message,
        stack: err?.stack
      },
      getUserError
    }
  })
}
