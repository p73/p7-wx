import { requestGet, requestPost, requestPut, requestWxLogin } from './wxPromise'
import { addDays } from './date'
import { config } from './config'
import { uuidv4 } from './crypto'
import { collectionFrom } from './db'
import { dbCollections } from './constants'
import { handleErrorLogging } from './error'

export const hasLoggedInBefore = async () => {
  try {
    const id = wx.getStorageSync(config.storageKey.wxOpenId)
    const users = await requestGet(`/users-permissions/search/${id}`)
    if (users.length) {
      return users[0]
    }
    return null
  } catch (e) {
    handleErrorLogging(e)
  }
}

export const getUserData = async (token) => {
  try {
    if (!wx.getStorageSync(config.storageKey.wxUserData)) {
      let resp = await requestGet('/users/me', {
        Authorization: `Bearer ${token}`
      })
      if (!resp.id) return {} // error handling
      wx.setStorageSync(config.storageKey.wxUserData, resp)
    }
    return wx.getStorageSync(config.storageKey.wxUserData)
  } catch (e) {
    console.log(e)
    // special case, it's expected to error out
    // handleErrorLogging(e)
    return {}
  }
}

export const verifyToken = async () => {
  // check if they've been logged out of weixin first
  try {
    await requestWxLogin()
    // we always login into the system after request wx login
    // check if token exists
    const token = wx.getStorageSync(config.storageKey.token).trim()
    const tokenExpiration = wx.getStorageSync(config.storageKey.tokenExpiration)
    // token will expire after 30 days
    if (token && tokenExpiration && (new Date(tokenExpiration) - new Date()) / 1000 < 30 * 24 * 60 * 60) {
      // verify token
      try {
        if (collectionFrom(dbCollections.app).isEmpty()) {
          const resp = await getUserData(token)
          if (!resp.id) {
            wx.removeStorageSync(config.storageKey.token)
            wx.removeStorageSync(config.storageKey.tokenExpiration)
            return null
          } else {
            return token
          }
        }
        return token
      } catch (e) {
        console.error(e)
        wx.removeStorageSync(config.storageKey.token)
        wx.removeStorageSync(config.storageKey.tokenExpiration)
        return null
      }
    } else {
      wx.removeStorageSync(config.storageKey.token)
      wx.removeStorageSync(config.storageKey.tokenExpiration)
      return null
    }
  } catch (e) {
    handleErrorLogging(e)
  }
}

const register = async (id) => {
  try {
    let { jwt } = await requestPost(`/auth/local/register`, {
      username: id,
      email: id + '@email.com',
      provider: 'local',
      password: id,
      confirmed: true,
      blocked: false,
      role: 'Authenticated',
      user_type: 'Student',
      weixin_username: id
    })
    await requestPost(`/payments/bind/weixin`, {
      wxSessionKey: wx.getStorageSync(config.storageKey.wxSessionKey),
      wxOpenId: id
    }, {
      Authorization: `Bearer ${jwt}`
    })
    wx.setStorageSync(config.storageKey.token, jwt)
    wx.setStorageSync(config.storageKey.tokenExpiration, addDays(new Date(), 30))
    await Promise.all([
      requestPost(`/user-inventories`, {
        quantity: 1,
        coupon: 1,
      }, {
        Authorization: `Bearer ${jwt}`
      }),
      requestPost(`/user-inventories`, {
        quantity: 3,
        coupon: 27,
      }, {
        Authorization: `Bearer ${jwt}`
      })
    ])

    return jwt
  } catch (e) {
    handleErrorLogging(e)
  }
}

export const loginWithoutPhone = async () => {
  try {
    const id = wx.getStorageSync(config.storageKey.wxOpenId)
    const token = await verifyToken()
    if (token) return token
    // otherwise check if user exists
    if (await hasLoggedInBefore()) {
      // if yes, just login again with the phone number
      try {
        let { jwt } = await requestPost(`/auth/local`, {
          'identifier': id,
          'password': id,
        })
        wx.setStorageSync(config.storageKey.token, jwt)
        wx.setStorageSync(config.storageKey.tokenExpiration, addDays(new Date(), 30))
        await requestPost(`/payments/bind/weixin`, {
          wxSessionKey: wx.getStorageSync(config.storageKey.wxSessionKey),
          wxOpenId: id
        }, {
          Authorization: `Bearer ${jwt}`
        })
        return jwt
      } catch (e) {
        console.log(e)
        return await register(id)
      }
    } else {
      // otherwise you'll need to register the new account
      return await register(id)
    }
  } catch (e) {
    handleErrorLogging(e)
  }

}

export const bindPhone = async (res) => {
  let phone
  try {
    if (!res.encryptedData) {
      if (config.mode === 'DEV' && !wx.getStorageSync(config.storageKey.mockPhone)) {
        res = { encryptedData: uuidv4() }
        wx.setStorageSync(config.storageKey.mockPhone, res.encryptedData)
        phone = res.encryptedData
      }
    } else {
      const resp = await requestPost('/profile/decrypt/weixin', {
        sessionKey: wx.getStorageSync(config.storageKey.wxSessionKey),
        encryptedData: res.encryptedData,
        iv: res.iv
      })
      phone = resp.phoneNumber
    }

    const token = await verifyToken()
    if (!token) {
      console.error('token error')
      return
    }
    wx.setStorageSync(config.storageKey.collectedPhone, 1)
    await requestPost('/payments/bind/phone', {
      phone
    }, {
      Authorization: `Bearer ${token}`
    })
  } catch (e) {
    handleErrorLogging(e)
  }

}

export const login = async (res) => {
  // first check if token exists
  let phone
  if (!res.encryptedData) {
    if (config.mode === 'DEV' && !wx.getStorageSync(config.storageKey.mockPhone)) {
      res = { encryptedData: uuidv4() }
      wx.setStorageSync(config.storageKey.mockPhone, res.encryptedData)
      phone = res.encryptedData
    }
  } else {
    const resp = await requestPost('/profile/decrypt/weixin', {
      sessionKey: wx.getStorageSync(config.storageKey.wxSessionKey),
      encryptedData: res.encryptedData,
      iv: res.iv
    })
    phone = resp.phoneNumber
  }

  const token = await verifyToken()
  if (token) return token
  // otherwise check if user exists
  if ((await requestGet(`/users-permissions/search/${phone}`)).length) {
    // if yes, just login again with the phone number
    let { jwt } = await requestPost(`/auth/local`, {
      'identifier': phone,
      'password': phone,
    })
    wx.setStorageSync(config.storageKey.token, jwt)
    wx.setStorageSync(config.storageKey.tokenExpiration, addDays(new Date(), 30))
    await requestPost(`/payments/bind/weixin`, {
      wxSessionKey: wx.getStorageSync(config.storageKey.wxSessionKey),
      wxOpenId: wx.getStorageSync(config.storageKey.wxOpenId)
    }, {
      Authorization: `Bearer ${jwt}`
    })
    return jwt
  } else {
    // otherwise you'll need to register the new account
    let { jwt } = await requestPost(`/auth/local/register`, {
      'username': phone,
      'email': phone + '@email.com',
      'provider': 'local',
      'password': phone,
      'confirmed': true,
      'blocked': false,
      'role': 'Authenticated',
      'user_type': 'Student'
    })
    await requestPost(`/payments/bind/weixin`, {
      wxSessionKey: wx.getStorageSync(config.storageKey.wxSessionKey),
      wxOpenId: wx.getStorageSync(config.storageKey.wxOpenId)
    }, {
      Authorization: `Bearer ${jwt}`
    })
    wx.setStorageSync(config.storageKey.token, jwt)
    wx.setStorageSync(config.storageKey.tokenExpiration, addDays(new Date(), 30))
    let toReceive = 0
    let signUpBonusCheck = wx.getStorageSync(config.storageKey.wxNewUserReceivedSignUpBonus)
    if (!signUpBonusCheck) {
      toReceive = 0
    } else {
      signUpBonusCheck = JSON.parse(signUpBonusCheck)
      toReceive = signUpBonusCheck.hasReceived ? 0 : 1
      wx.setStorageSync(config.storageKey.wxNewUserReceivedSignUpBonus, JSON.stringify({
        hasReceived: true
      }))
    }
    await Promise.all([
      requestPost(`/user-inventories`, {
        quantity: 1 + toReceive,
        coupon: 1,
      }, {
        Authorization: `Bearer ${jwt}`
      }),
      requestPost(`/user-inventories`, {
        quantity: 3,
        coupon: 27,
      }, {
        Authorization: `Bearer ${jwt}`
      })
    ])
    return jwt
  }
}

export const bindUserProfile = async () => {
  try {
    const token = await verifyToken()
    const resp = await getUserData(token)
    let userProfile = JSON.parse(wx.getStorageSync(config.storageKey.wxUserProfile))
    await requestPut(`/users/${resp.id}`, {
      real_name: userProfile.nickName,
      rawUserProfileJson: userProfile
    }, {
      Authorization: `Bearer ${token}`
    })
  } catch (e) {
    console.log(e)
    // handleErrorLogging(e)
  }
}

export const versionCheck = async () => {
  // always clean user data, pull new one when start up
  wx.removeStorageSync(config.storageKey.wxUserData)
  switch (wx.getStorageSync(config.storageKey.version)) {
    case '1.9.10':
      return
    case '1.9.6':
    case '1.9.5':
    case '1.9.2':
    case '1.9.0':
    case '1.8.11':
    case '1.8.8':
    case '1.8.4':
    case '1.6.6':
    case '1.4.2':
    case '1.0.1':
    // fix major bugs
    case '0.0.4':
    // major changes to versioning and how user login works
    case '0.0.5':
    // only minor ui changes
    case '0.0.6':
    // more ui changes
    case '0.0.7':
    // fix coupon decrease and calendar page logic
    case '1.0.0':
    // full function, but needs reset
    default:
      // upload userprofile data
      await bindUserProfile()
      // don't clear everything
      wx.removeStorageSync(config.storageKey.version)
      wx.removeStorageSync(config.storageKey.wxOpenId)
      wx.removeStorageSync(config.storageKey.token)
      wx.removeStorageSync(config.storageKey.wxOpenIdCode)
      wx.removeStorageSync(config.storageKey.wxSessionKey)
      wx.removeStorageSync(config.storageKey.wxUserData)
      wx.setStorageSync(config.storageKey.version, config.version)
  }
}
